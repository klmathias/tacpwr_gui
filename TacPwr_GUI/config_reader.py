import sqlite3
import gui_client as gc

database_path = '../controller/TacPwrData/db/tacpwr.db'

config_data = {}

last_read = None

def read(ipdu):
    global last_read
    if ipdu != 0:
        global config_data
        try:
            connection = sqlite3.connect(database_path)
            cursor = connection.cursor()
            cursor.execute(f'SELECT * FROM CONFIG WHERE IPDU_ID = {ipdu};')
            results = cursor.fetchall()[0]
        except:
            return
        config_data[ipdu] = {
            'IPDU_ID':                  results[0],
            'MAIN_L1_VOLT_MAX':         results[1],
            'MAIN_L1_VOLT_MIN':         results[2],
            'MAIN_L2_VOLT_MAX':         results[3],
            'MAIN_L2_VOLT_MIN':         results[4],
            'MAIN_L3_VOLT_MAX':         results[5],
            'MAIN_L3_VOLT_MIN':         results[6],
            'MAIN_L1_FREQ_MAX':            results[7],
            'MAIN_L1_FREQ_MIN':            results[8],
            'MAIN_L2_FREQ_MAX':            'N/A',
            'MAIN_L2_FREQ_MIN':            'N/A',
            'MAIN_L3_FREQ_MAX':            'N/A',
            'MAIN_L3_FREQ_MIN':            'N/A',
            'MAIN_L1_L2_PHASE_MAX':           results[9],
            'MAIN_L1_L2_PHASE_MIN':           results[10],
            'MAIN_L2_L3_PHASE_MAX':           'N/A',
            'MAIN_L2_L3_PHASE_MIN':           'N/A',
            'MAIN_L3_L1_PHASE_MAX':           'N/A',
            'MAIN_L3_L1_PHASE_MIN':           'N/A',
            'MAIN_L1_AMP_MAX':          results[11],
            'MAIN_L2_AMP_MAX':          results[12],
            'MAIN_L3_AMP_MAX':          results[13],
            'MAIN_N_AMP_MAX':           results[14],
            '60A_L1_AMP_MAX':          results[15],
            '60A_L2_AMP_MAX':          results[16],
            '60A_L3_AMP_MAX':          results[17],
            '60A_N_AMP_MAX':           results[18],
            '60B_L1_AMP_MAX':          results[19],
            '60B_L2_AMP_MAX':          results[20],
            '60B_L3_AMP_MAX':          results[21],
            '60B_N_AMP_MAX':          results[22],
            '40A_L1_AMP_MAX':          results[23],
            '40A_L2_AMP_MAX':          results[24],
            '40A_L3_AMP_MAX':          results[25],
            '40A_N_AMP_MAX':            results[26],
            '40B_L1_AMP_MAX':          results[27],
            '40B_L2_AMP_MAX':          results[28],
            '40B_L3_AMP_MAX':          results[29],
            '40B_N_AMP_MAX':           results[30],
            '20A_L1_AMP_MAX':          results[31],
            '20A_N_AMP_MAX':           results[32],
            '20B_L2_AMP_MAX':          results[33],
            '20B_N_AMP_MAX':           results[34],
            'BYP_L1_AMP_MAX':          results[35],
            'BYP_L2_AMP_MAX':          results[36],
            'BYP_L3_AMP_MAX':          results[37],
            'BYP_N_AMP_MAX':           results[38],

            'OVER_SUB_MAIN_AMP':       results[39],
            'OVER_SUB_60A_AMP':        results[40],
            'OVER_SUB_60B_AMP':        results[41],
            'OVER_SUB_40A_AMP':        results[42],
            'OVER_SUB_40B_AMP':        results[43],
            'OVER_SUB_20A_AMP':        results[44],
            'OVER_SUB_20B_AMP':        results[45],

            'MAIN_60A_PRIORITY':        results[46],
            'MAIN_60B_PRIORITY':        results[47],
            'MAIN_40A_PRIORITY':        results[48],
            'MAIN_40B_PRIORITY':        results[49],
            'MAIN_20A_PRIORITY':        results[50],
            'MAIN_20B_PRIORITY':        results[51],
            'inputFrquency':            results[52],
            'ledState':                 results[53],
            'feedControlState':         results[54],

            'MAIN_L1_VOLT_SNAPSHOT':        results[55],
            'MAIN_L2_VOLT_SNAPSHOT':        results[56],
            'MAIN_L3_VOLT_SNAPSHOT':        results[57],
            'MAIN_L1_AMP_SNAPSHOT':        results[58],
            'MAIN_L2_AMP_SNAPSHOT':        results[59],
            'MAIN_L3_AMP_SNAPSHOT':        results[60],
            'MAIN_N_AMP_SNAPSHOT':        results[61],
            '60A_L1_AMP_SNAPSHOT':    results[62],
            '60A_L2_AMP_SNAPSHOT':    results[63],
            '60A_L3_AMP_SNAPSHOT':    results[64],
            '60A_N_AMP_SNAPSHOT':     results[65],
            '60B_L1_AMP_SNAPSHOT':    results[66],
            '60B_L2_AMP_SNAPSHOT':    results[67],
            '60B_L3_AMP_SNAPSHOT':    results[68],
            '60B_N_AMP_SNAPSHOT':     results[69],
            '40A_L1_AMP_SNAPSHOT':    results[70],
            '40A_L2_AMP_SNAPSHOT':    results[71],
            '40A_L3_AMP_SNAPSHOT':    results[72],
            '40A_N_AMP_SNAPSHOT':     results[73],
            '40B_L1_AMP_SNAPSHOT':    results[74],
            '40B_L2_AMP_SNAPSHOT':    results[75],
            '40B_L3_AMP_SNAPSHOT':    results[76],
            '40B_N_AMP_SNAPSHOT':     results[77],
            '20A_L1_AMP_SNAPSHOT':    results[78],
            '20A_N_AMP_SNAPSHOT':     results[79],
            '20B_L2_AMP_SNAPSHOT':    results[80],
            '20B_N_AMP_SNAPSHOT':     results[81],
            'BYP_L1_AMP_SNAPSHOT':    results[82],
            'BYP_L2_AMP_SNAPSHOT':    results[83],
            'BYP_L3_AMP_SNAPSHOT':    results[84],
            'BYP_N_AMP_SNAPSHOT':     results[85],
            'MAIN_L1_VOLT_REALTIME':               results[86],
            'MAIN_L2_VOLT_REALTIME':               results[87],
            'MAIN_L3_VOLT_REALTIME':               results[88],
            'noise':                    results[89],
            'MAIN_L1_AMP_REALTIME':              results[90],
            'MAIN_L2_AMP_REALTIME':              results[91],
            'MAIN_L3_AMP_REALTIME':              results[92],
            'MAIN_N_AMP_REALTIME':               results[93],
            '60A_L1_AMP_REALTIME':              results[94],
            '60A_L2_AMP_REALTIME':              results[95],
            '60A_L3_AMP_REALTIME':              results[96],
            '60A_N_AMP_REALTIME':               results[97],
            '60B_L1_AMP_REALTIME':              results[98],
            '60B_L2_AMP_REALTIME':              results[99],
            '60B_L3_AMP_REALTIME':              results[100],
            '60B_N_AMP_REALTIME':               results[101],
            '40A_L1_AMP_REALTIME':              results[102],
            '40A_L2_AMP_REALTIME':              results[103],
            '40A_L3_AMP_REALTIME':              results[104],
            '40A_N_AMP_REALTIME':               results[105],
            '40B_L1_AMP_REALTIME':              results[106],
            '40B_L2_AMP_REALTIME':              results[107],
            '40B_L3_AMP_REALTIME':              results[108],
            '40B_N_AMP_REALTIME':               results[109],
            '20A_L1_AMP_REALTIME':              results[110],
            '20A_N_AMP_REALTIME':               results[111],
            '20B_L2_AMP_REALTIME':              results[112],
            '20B_N_AMP_REALTIME':               results[113],
            'BYP_L1_AMP_REALTIME':              results[114],
            'BYP_L2_AMP_REALTIME':              results[115],
            'BYP_L3_AMP_REALTIME':              results[116],
            'BYP_N_AMP_REALTIME':               results[117],


            'MAIN_L1_FREQ_SNAPSHOT':            'N/A',
            'MAIN_L2_FREQ_SNAPSHOT':            'N/A',
            'MAIN_L3_FREQ_SNAPSHOT':            'N/A',
            'MAIN_L1_L2_PHASE_SNAPSHOT':           'N/A',
            'MAIN_L2_L3_PHASE_SNAPSHOT':           'N/A',
            'MAIN_L3_L1_PHASE_SNAPSHOT':           'N/A',

            'MAIN_L1_FREQ_REALTIME':            'N/A',
            'MAIN_L2_FREQ_REALTIME':            'N/A',
            'MAIN_L3_FREQ_REALTIME':            'N/A',
            'MAIN_L1_L2_PHASE_REALTIME':           'N/A',
            'MAIN_L2_L3_PHASE_REALTIME':           'N/A',
            'MAIN_L3_L1_PHASE_REALTIME':           'N/A',

            # 'MAIN_L1_FREQ_MAX': 'N/A',
            'MAIN_L2_FREQ_MAX': 'N/A',
            'MAIN_L3_FREQ_MAX': 'N/A',


            'MAIN_L1_AMP_MIN':          'N/A',
            'MAIN_L2_AMP_MIN':          'N/A',
            'MAIN_L3_AMP_MIN':          'N/A',
            'MAIN_N_AMP_MIN':           'N/A',
            '60A_L1_AMP_MIN':          'N/A',
            '60A_L2_AMP_MIN':          'N/A',
            '60A_L3_AMP_MIN':          'N/A',
            '60A_N_AMP_MIN':           'N/A',
            '60B_L1_AMP_MIN':          'N/A',
            '60B_L2_AMP_MIN':          'N/A',
            '60B_L3_AMP_MIN':          'N/A',
            '60B_N_AMP_MIN':          'N/A',
            '40A_L1_AMP_MIN':          'N/A',
            '40A_L2_AMP_MIN':          'N/A',
            '40A_L3_AMP_MIN':          'N/A',
            '40A_N_AMP_MIN':          'N/A',
            '40B_L1_AMP_MIN':          'N/A',
            '40B_L2_AMP_MIN':          'N/A',
            '40B_L3_AMP_MIN':          'N/A',
            '40B_N_AMP_MIN':           'N/A',
            '20A_L1_AMP_MIN':          'N/A',
            '20A_N_AMP_MIN':           'N/A',
            '20B_L2_AMP_MIN':          'N/A',
            '20B_N_AMP_MIN':           'N/A',
            'BYP_L1_AMP_MIN':          'N/A',
            'BYP_L2_AMP_MIN':          'N/A',
            'BYP_L3_AMP_MIN':          'N/A',
            'BYP_N_AMP_MIN':           'N/A'
        }
        cursor.close()
        connection.close()
        last_read = ipdu
