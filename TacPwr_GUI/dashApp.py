from logging import PlaceHolder
import dash
from dash import html
from dash.html.S import S
import dash_bootstrap_components as dbc
from dash import dcc
import dash_daq as daq
import pandas as pd
import plotly.graph_objs as go
import plotly.express as px
from dash.dependencies import Input, Output, State
import sys
import threading
import gui_client as gc
import message_parser as mp
import realtime_data_reader as rtd
import config_reader as cfg
from os import listdir
from os import path
from os import rename
from os import remove
from shutil import copy2
from pathlib import Path
import time
import ctypes
import struct
import socket
import structures as strs

data_path = '../controller/TacPwrData/data/'
prev_filename = None
max_snapshot_history = 25
selected_ipdu = None

thread_started = False

rtd_count = {}
rtd_message_queue = []

event_history = []

theme = 'light'
ignored_files = '.*dark.*'
if '-dark' in sys.argv:
    theme = 'dark'
    ignored_files = '.*light.*'

theme_colors = {
    'light': {
        'primary': '#FFFFFF',
        'secondary': '#EEEEEE',
        'success': '#28a745',
        'danger': '#dc3545',
        'warning': '#ffc107',
        'info': '#17a2b8',
        'light': '#f8f9fa',
        'dark': '#343a40',
        'white': '#FFFFFF'},

    'dark': {
        'primary': '#343a40',
        'secondary': '#1b1e21',
        'success': '#68CFBC',
        'danger': '#CF6679',
        'warning': '#DAC365',
        'info': '#687BCF',
        'light': '#101214',
        'dark': '#E0E0E0',
        'white': '#272727'}
}

template = {
    'light': 'plotly_white',
    'dark': 'plotly_dark'}


#Other background options: #272727
waveform_layout = None
main_snapshot_layout = None
snapshot_select_layout = None
def update_figure_layouts():
    global waveform_layout
    global main_snapshot_layout
    global snapshot_select_layout
    waveform_layout = go.Layout(template=template[theme], paper_bgcolor=theme_colors[theme]['white'], plot_bgcolor=theme_colors[theme]['white'], margin=dict(l=30, r=30, t=0, b=20))
    main_snapshot_layout = go.Layout(height=270, margin=dict(l=30, r=30, t=0, b=20), yaxis = {'fixedrange': True, 'rangemode': 'tozero'}, xaxis={'fixedrange' : True, 'range': [0,max_snapshot_history-1]}, template=template[theme], paper_bgcolor=theme_colors[theme]['white'], plot_bgcolor=theme_colors[theme]['white'])
    snapshot_select_layout = go.Layout(yaxis = {'fixedrange': True, 'rangemode': 'tozero'}, xaxis={'fixedrange' : True, 'range': [0,max_snapshot_history-1]}, template=template[theme], paper_bgcolor=theme_colors[theme]['white'], plot_bgcolor=theme_colors[theme]['white'],  margin=dict(l=30, r=30, t=0, b=20),)

update_figure_layouts()


ideal_voltage = 120

#Min/max range of voltage gauges
voltage_gauge_min = 0
voltage_gauge_max = 160

#Upper/lower alarm limits
voltage_gauge_alarm_lower = 100
voltage_gauge_alarm_upper = 130

#Percentage of upper/lower alarm limit to be yellow warning
voltage_gauge_alarm_lower_warning_percent = .05
voltage_gauge_alarm_upper_warning_percent = .05

voltage_gauge_yellow_lower = voltage_gauge_alarm_lower - (voltage_gauge_alarm_lower_warning_percent * ideal_voltage)
voltage_gauge_yellow_upper = voltage_gauge_alarm_upper + (voltage_gauge_alarm_upper_warning_percent * ideal_voltage)

gauge_data = None

selected_snapshot_feed = None


#Dictionary for bootstrap colors
colors = {
    'primary': '#bb86fc',
    'secondary': '#03dac6',
    'success': '#28a745',
    'danger': '#dc3545',
    'warning': '#ffc107',
    'info': '#17a2b8',
    'light': '#121212',
    'dark': '#343a40',
    'white': '#1d1d1d',
    'grey': '#e6e6e6'
}

daq_theme = {
    'dark': False,
    'detail': '#007439',
    'primary': theme_colors[theme]['secondary'],
    'secondary': theme_colors[theme]['secondary'],
}

# theme = dbc.themes.MINTY
app = dash.Dash(__name__, update_title=None, assets_ignore=ignored_files)

#Surpress callback exceptions
app.config.suppress_callback_exceptions = False

# controller_ip = sys.argv[1] if len(sys.argv) >= 2 else 'localhost'
# controller_port = sys.argv[2] if len(sys.argv) >= 3 else '8090'


alarm_table = html.Table(id='alarms-tbl', children=[
    html.Tr(className='alarm-header',children=[
        html.Th(className='alarm-th', children=[
            html.Span(className='text-danger',children='Breaker')
        ]),
        html.Th(className='alarm-th', children=[
            html.Span(className='text-warning',children='Shed')
        ]),
        html.Th(className='alarm-th', children=[
            html.Span(children='Circuit', className='text-dark')
        ]),
        html.Th(className='alarm-th', children=[
            html.Span(children='Priority', className='text-dark')
        ]),
        html.Th(className='alarm-th', children=[
            html.Span(className='text-warning',children='Manual Shed')
        ]),
    ]),
    html.Tr(children=[
        html.Td(className='alarm-td', children=[daq.Indicator(id='MAIN-breaker-indicator', color='#AAA')]),
        html.Td(className='alarm-td'),
        html.Td(className='alarm-td', children=['MAIN']),
        html.Td(className='alarm-td'),
        html.Td(className='alarm-td')
    ]),
    *[html.Tr(children=[
        html.Td(className='alarm-td', children=[daq.Indicator(id=f'{circuit}-breaker-indicator', color='#AAA')]),
        html.Td(className='alarm-td', children=[daq.Indicator(id=f'{circuit}-contactor-indicator', color='#AAA')]),
        html.Td(className='alarm-td', children=[f'{circuit}']),
        html.Td(className='alarm-td', children=[html.Span(dcc.Input(id=f'{circuit}_priority_input', max=255, min=0, className='priority-input bg-secondary text-dark', type='number', inputMode='numeric'), id=f'tooltip_target_{circuit}')]),
        dbc.Tooltip('0: Highest | 255: Lowest', target=f'tooltip_target_{circuit}'),
        html.Td(className='alarm-td', children=[daq.BooleanSwitch(id=f'{circuit}_manual_shed', color=theme_colors[theme]['warning'])])
    ]) for circuit in strs.circuits],
    html.Tr(children=[
        html.Td(className='alarm-td'),
        html.Td(className='alarm-td', children=[daq.Indicator(id='BYP-contactor-indicator', color='#AAA')]),
        html.Td(className='alarm-td', children=['TEST']),
        html.Td(className='alarm-td'),
        html.Td(className='alarm-td', children=[daq.BooleanSwitch(id='BYP_manual_shed', color=theme_colors[theme]['warning'])])
    ]),
]) #End alarm / priority table

waveform_select_dropdowns = [*[
    dcc.Dropdown(id=f'waveform-select-{circuit}', value=[], style={'background-color': theme_colors[theme]['secondary']}, placeholder=f'{circuit}', multi=True, options=[
        *[{'label':f'{circuit} {line}', 'value':f'{circuit}_{line}_AMP'} for line in strs.get_lines[circuit]],
    ]) for circuit in strs.circuits + ['MAIN', 'BYP']],
    dcc.Dropdown(id='waveform-select-VOLT', value=[], style={'background-color':theme_colors[theme]['secondary']}, placeholder='Volt', multi=True, options=[
        *[{'label':f'Voltage {line}', 'value':f'MAIN_{line}_VOLT'} for line in ['L1','L2','L3']]
    ])]

breaker_indicators_value_outputs = [Output(f'{circuit}-breaker-indicator', 'value') for circuit in strs.circuits + ['MAIN']]
contactor_indicators_value_outputs = [Output(f'{circuit}-contactor-indicator', 'value') for circuit in strs.circuits + ['BYP']]
breaker_indicators_color_outputs = [Output(f'{circuit}-breaker-indicator', 'color') for circuit in strs.circuits + ['MAIN']]
contactor_indicators_color_outputs = [Output(f'{circuit}-contactor-indicator', 'color') for circuit in strs.circuits + ['BYP']]

config_data_inputs =[State(f'{feed}_SNAPSHOT_INPUT', 'value') for feed in strs.feeds] +\
                    [State(f'{feed}_REALTIME_INPUT', 'value') for feed in strs.feeds] +\
                    [State(f'{feed}_MAX_INPUT', 'value') for feed in strs.feeds] +\
                    [State(f'{feed}_MIN_INPUT', 'value') for feed in strs.feeds] +\
                    [State(f'OVER_SUB_{circuit}_INPUT', 'value') for circuit in ['MAIN'] + strs.circuits] +\
                    [State(f'CONFIG_{circuit}_PRIORITY', 'value') for circuit in strs.circuits]

config_data_table =[
    html.Tr([
        html.Td(strs.common_name[feed]),
        html.Td(dcc.Input(id=f'{feed}_SNAPSHOT_INPUT', type='number', inputMode='numeric', value=None, disabled=True, className='bg-secondary text-dark config-td text-right')),
        html.Td(dcc.Input(id=f'{feed}_REALTIME_INPUT', type='number', inputMode='numeric', value=None, disabled=True, className='bg-secondary text-dark config-td text-right')),
        html.Td(dcc.Input(id=f'{feed}_MAX_INPUT', type='number', inputMode='numeric', value=None, disabled=True, className='bg-secondary text-dark config-td text-right')),
        html.Td(dcc.Input(id=f'{feed}_MIN_INPUT', type='number', inputMode='numeric', value=None, disabled=True, className='bg-secondary text-dark config-td text-right')),
    ]) for feed in strs.feeds]
config_data_table.insert(0,
    html.Tr([
    html.Th('Feed'),
    html.Th('Snapshot'),
    html.Th('Realtime'),
    html.Th('High Alarm'),
    html.Th('Low Alarm')
]))

config_oversubscription_table = [
    html.Tr([
        html.Td(circuit),
        html.Td(dcc.Input(id=f'OVER_SUB_{circuit}_INPUT', type='number', inputMode='numeric', value=None, disabled=True, className='bg-secondary text-dark config-td float-right text-right'))
    ]) for circuit in ['MAIN'] + strs.circuits]
        
config_priority_table = [
    html.Tr(className='alarm-header',children=[
        html.Th(className='alarm-th', children=[
            html.Span(children='Circuit', className='text-dark')
        ]),
        html.Th(className='alarm-th', children=[
            html.Span(children='Priority', className='text-dark')
        ]),
        html.Th(className='alarm-th', children=[
            html.Span(className='text-warning',children='Manual Shed')
        ]),
    ]),
    *[html.Tr([
        html.Td(circuit),
        html.Td(dcc.Input(id=f'CONFIG_{circuit}_PRIORITY', type='number', inputMode='numeric', value=None, disabled=True, className='bg-secondary text-dark config-td float-right text-right')),
        html.Td(className='alarm-td', children=[daq.BooleanSwitch(id=f'CONFIG_{circuit}_manual_shed', color=theme_colors[theme]['warning'])])
    ]) for circuit in strs.circuits],
    html.Tr([
        html.Td('TEST'),
        html.Td(),
        html.Td(className='alarm-td', children=[daq.BooleanSwitch(id='CONFIG_BYP_manual_shed', color=theme_colors[theme]['warning'])])
    ])    
]

config_layout = []
def update_config_layout():
    global config_layout
    config_layout = html.Div(id='main', children=[
        #Contains main page content
        html.Div(id='content-container', className='bg-light', children=[
            html.Div(className='d-flex flex-column bg-light',children=[ #Flex column for main content panels
                html.Div(className='d-flex flex-row justify-content-center',children=[ 
                    html.Div([ #Selection panel container
                        html.Div(id='config_oversubscription_table_container', className='m-2 p-2 rounded bg-white shadow', children=config_oversubscription_table),
                        html.Div(id='config_priority_table_container', className='m-2 p-2 rounded bg-white shadow', children=config_priority_table),
                        html.Div(id='config-select-panel', className='m-2 p-2 rounded bg-white shadow', children=[ #Configuration selection panel
                            html.Div('Select Frequency', className='small-header text-dark'),
                            dcc.RadioItems(id='freq_select_radio_items', 
                                options=[
                                    {'label': '60 Hz', 'value':'60'},
                                    {'label': '50 Hz', 'value':'50'}
                                ],
                                value='60',
                                labelStyle={'display':'block'},
                                labelClassName='alarm-td',
                                inputClassName='alarm-td color-info radio-input'
                            ),
                            html.Hr(),
                            html.Div('IPDU External LEDs', className='small-header text-dark'),
                            dcc.RadioItems(id='led_select_radio_items', 
                                options=[
                                    {'label': 'On', 'value': 'On'},
                                    {'label': 'Off', 'value': 'Off'}
                                ],
                                value='On',
                                labelStyle={'display':'block'},
                                labelClassName='alarm-td',
                                inputClassName='alarm-td color-info radio-input'
                            ),
                            html.Hr(),
                            html.Div(html.Button('Submit', id='config-submit-btn', className='btn btn-info w-100'), className='d-inline-block w-50 p-1'),
                            html.Div(html.Button('Refresh', id='config-refresh-btn', className='btn btn-info w-100'), className='d-inline-block w-50 p-1'),
                        ]) # End configuration selection panel
                    ]), # End configuration selection panel container
                    html.Div(id='config_data_table_container', className='p-2 m-2 rounded bg-white shadow', children=config_data_table)
                ])
            ])
        ]),
        html.Div(id='config-dummy', style={'display':'none'})
    ])
update_config_layout()

#Main app layout
main_layout = []
def update_main_layout():
    global main_layout
    main_layout = html.Div(id='content-container', className='bg-light', children=[
        html.Div(id='voltage', className='m-2 p-2', children=[ #Voltage Panel
            html.Div(className='d-flex flex-row justify-content-between',children=[ 
                html.Div(className='rounded m-2 p-2 bg-primary shadow align-self-center flex-shrink-0', children=[ #Alarms Panel Vertical Flex Containernter', children=[
                    html.Div('Feed Control', className='small-header text-dark'),
                    alarm_table,
                    html.Div(html.Button('Submit', id='submit-btn', className='btn btn-info w-100'), className='d-inline-block w-50 p-1'),
                    html.Div(html.Button('Refresh', id='refresh-feed-control-btn', className='btn btn-info w-100'), className='d-inline-block w-50 p-1')
                ]),#End alarm panel
                
                    html.Div(id='gauges', className='d-flex flex-column m-2 p-2 rounded bg-white shadow', children=[
                        html.Div(children=[
                            html.H2('Main Voltage', className='panel-header text-dark'),
                            html.Div(className=('m-0 p-0 rounded bg-white overflow-hidden'), children=[
                                dcc.Graph(id='main-voltage-graph', figure=go.Figure(layout=main_snapshot_layout))
                            ]),
                        ]), #End voltage graph
                        
                        html.Br(),

                        html.Div(children=[
                            html.H2('Main Current', className='panel-header text-dark'),
                            html.Div(className=('p-0 m-0 rounded bg-white overflow-hidden'), children=[
                                dcc.Graph(id='main-current-graph', figure=go.Figure(layout=main_snapshot_layout))
                            ]),
                        ]) #End current graph
                        
                    ]), #End main graph panel

                    html.Div(id='main-info-panel', className='rounded m-2 p-2 slow-grow bg-white shadow align-self-center flex-shrink-0', children=[
                        html.Div('Info', className='small-header text-dark'),
                        html.Div(id='main-info-table')
                    ]) #End main info panel
            ]), #End voltage / info panel flex row
        ]), # End main voltage/current section

        #Multi-use display panel
        html.Div(id='display', className='active grow', children=[
            dcc.Tabs(id='display-tabs', value='waveform', className='bg-secondary', children=[
                dcc.Tab(label='Realtime Data', value='waveform', className='border bg-secondary rounded-top', selected_className='border border-bottom-0 text-info'),
                dcc.Tab(label='Snapshot Data', value='snapshot', className='border bg-secondary rounded-top', selected_className='border border-bottom-0 text-info')
            ]),
            html.Div(id='waveform',className='p-2 border-top bg-secondary panel',children=[ #Waveform display
                html.Div(className='d-flex flex-row justify-content-between h-100 bg-secondary',children=[ #Waveform display flex container

                    html.Div(id='waveform-select-panel', className='m-2 p-2 rounded bg-primary shadow align-self-center flex-shrink-0', children=[ #Waveform select panel
                        html.Div('Waveform Select', className='small-header text-dark'),
                        dcc.Dropdown(id='waveform-select-dropdown', placeholder='Select File...', style={'background-color': theme_colors[theme]['secondary']}),
                        html.Button('Request New Data', id='realtime-request-btn', className='btn btn-info w-100 mt-1'),
                        html.Hr(),
                        *waveform_select_dropdowns,
                    ]),#End waveform select panel
                    
                    html.Div(className=('m-2 p-2 rounded w-100 bg-white shadow overflow-hidden'), children=[
                        html.H2('No File Selected', id='rtd-filename', className='text-center panel-header text-dark'),
                        html.Div(className=('m-0 p-0 rounded bg-white overflow-hidden'),children=[
                            dcc.Graph(id='waveform-graph',figure=go.Figure(layout=waveform_layout))
                        ])
                    ]),
                    
                    html.Div(id='waveform-rms-panel', className='m-2 p-2 rounded bg-white shadow align-self-center flex-shrink-0', children=[
                        html.Div('RMS', className='small-header text-dark'),
                        html.Div('Select waveforms to view RMS values.', id='waveform-rms-panel-content')
                    ])#Waveform select container
                ]) # End waveform flex row
            ]), #End waveform panel
            html.Div(id='snapshot',className='p-2 border-top bg-secondary panel', children=[
                html.Div(className='d-flex flex-row justify-content-between h-100 bg-secondary',children=[
                    html.Div(id='snapshot-select-panel', className='m-2 p-2 rounded bg-primary shadow align-self-center flex-shrink-0', children=[
                        html.Div('Snapshot Select', className='small-header text-dark'),
                        dcc.RadioItems(id='snapshot_select_radio_items', 
                            options=[
                                {'label': '60A', 'value':'60A'},
                                {'label': '60B', 'value':'60B'},
                                {'label': '40A', 'value':'40A'},
                                {'label': '40B', 'value':'40B'},
                                {'label': '20A', 'value':'20A'},
                                {'label': '20B', 'value':'20B'},
                                {'label': 'BYPASS', 'value':'BYP'},
                            ],
                            value='60A',
                            labelStyle={'display':'block'},
                            labelClassName='alarm-td',
                            inputClassName='alarm-td color-info radio-input'
                        )
                    ]),#End snapshot select panel
                    html.Div(className=('m-2 p-2 rounded bg-white w-100 shadow overflow-hidden'), children=[
                        html.H2('60A', id='snapshot_select_gauges_title', className='panel-header text-dark'),
                        dcc.Graph(id='snapshot-graph', figure=go.Figure(layout=snapshot_select_layout))
                    ]),#End inner snapshot gauges panel
                    
                    html.Div(id='snapshot-select-info-panel', className='m-2 p-2 rounded bg-white shadow align-self-center flex-shrink-0', children=[
                        html.Div('Snapshot Info', className='small-header text-dark'),
                        html.Table(id='snapshot-select-info-table', className='w-100'),
                        html.Hr(),
                        html.Table(id='snapshot-select-powerfactor-table', className='w-100')
                    ])#End snapshot select panel
                ])#End snapshot flex row
            ]) #End snapshot feed panel
        ]),
        html.Div(id='dummy', style={'display':'none'}),
        html.Div(id='refresh-dummy', style={'display':'none'}),
        html.Div(id='submit-dummy', style={'display':'none'}),
        html.Div(id='request-dummy', style={'display':'none'}), #End multi-use display panel
    ]),#end content container    

update_main_layout()

#Empty layout, callback will load correct page
app.layout = daq.DarkThemeProvider(theme=daq_theme, children=[html.Div([
        dcc.Location(id='url', refresh=False),

        html.Div(id='header', className='d-flex flex-row w-100 p-2 m-0 justify-content-between rounded-bottom center-vertical shadow navbar navbar-expand-lg navbar-dark bg-primary',children=[
            html.Table(html.Tr([
                html.Td(html.A(html.Img(src=f'/assets/icons/{theme}/house.svg', className='house-icon'),href='./')),
                html.Td(dcc.Dropdown(id='ipdu-dropdown', persistence=True, clearable=False, placeholder='No IPDUs Available', style={'background-color': theme_colors[theme]['secondary']}), className='pl-4')
            ])),

            html.Div(children=[
                html.Div(id='waiting_connection', children=[])
            ]),
            html.Table([
                html.Tr([
                    html.Td(html.Table([html.Tr([html.Td(id='ipdu-time'), html.Td(id='ipdu-temperature', className='pl-4 pr-4')]), html.Tr([html.Td(id='embedded-software-version'), html.Td(id='fpga-software-version')])])),
                    html.Td(html.Img(src=f'/assets/icons/{theme}/notification.svg', id='notification-icon', n_clicks=0, style={'cursor': 'pointer'})),
                    html.Td(html.A(className='d-inline-block m-0 p-0', href='./config',children=[html.Img(src=f'/assets/icons/{theme}/gear.svg', className='gear-icon')]))
            ])]),
        ]), # End header
        html.Div(id='page-content'),
        html.H4(id='alarms-btn', className='d-none', children=[
            dbc.Badge('0', color='warning', id='alarm-count-badge', pill=True, className='border'),
            html.Img(src=f'/assets/icons/{theme}/exclamation-circle.svg', id='open', n_clicks=0, className='alarm-icon')
        ]),
        html.Div([], id='message-box'),
        dbc.Modal(
            [
                dbc.ModalHeader('Alarms', className='text-dark'),
                dbc.ModalBody(id='alarm-body'),
                dbc.ModalFooter(
                    dbc.Button(
                        'Close', id='close', className='ml-auto btn-danger', n_clicks=0
                    )
                ),
            ],
            id="modal",
            is_open=False,
            scrollable=True
        ),
        
        dbc.Modal(
            [
                dbc.ModalHeader('Event History', className='text-dark'),
                dbc.ModalBody(id='event-history-modal-body'),
                dbc.ModalFooter(
                    dbc.Button(
                        'Close', id='event-history-modal-close', className='ml-auto btn-danger', n_clicks=0
                    )
                ),
            ],
            id='event-history-modal',
            is_open=False,
            scrollable=True
        ),
        html.Div(id='page-content-dummy', style={'display': 'none'}),
        html.Div(id='theme-dummy', className='d-none'),
        html.Div(id='dropdown-dummy', className='d-none'),
        html.Div(id='init-priority-dummy', className='d-none'),
        dcc.Interval(id='update_interval', interval=1000),
    ]) 
])

@app.callback(
    Output('ipdu-dropdown', 'options'),
    Output('ipdu-dropdown', 'disabled'),
    Output('ipdu-dropdown', 'placeholder'),
    Input('update_interval', 'n_intervals'),
    Input('ipdu-dropdown', 'value'))
def update_ipdu_dropdown(interval, val):
    global selected_ipdu
    options = [{'label':f'{ipdu}', 'value':strs.ipdus.index(ipdu)} for ipdu in strs.ipdus]
    is_disabled = (options is None or not len(options))
    placeholder = 'Select IPDU...' if options and len(options) else 'No IPDUs Available'
    selected_ipdu = strs.ipdus[val] if val is not None and val < len(strs.ipdus) else None
    return options, is_disabled, placeholder
    
@app.callback(
    Output('ipdu-temperature', 'children'),
    Output('ipdu-time', 'children'),
    Output('embedded-software-version', 'children'),
    Output('fpga-software-version', 'children'),
    Input('update_interval', 'n_intervals'))
def update_ipdu_info(interval):
    if selected_ipdu:
        return f'Temperature: {mp.ipdu_temperature}', time.ctime(), mp.embedded_software_version, mp.fpga_software_version
    return '', '', '', ''

@app.callback(
    Output('config_data_table_container', 'children'),
    Output('config_oversubscription_table_container', 'children'),
    Output('config_priority_table_container', 'children'),
    Output('led_select_radio_items', 'value'),
    Output('freq_select_radio_items', 'value'),
    Input('config-refresh-btn','n_clicks'),
    Input('ipdu-dropdown', 'value'))
def refresh_config(click, ipdu):
    global selected_ipdu
    if ipdu is not None and ipdu < len(strs.ipdus):
        selected_ipdu = strs.ipdus[ipdu]
        cfg.read(strs.ipdus[ipdu])

    if selected_ipdu in cfg.config_data and cfg.config_data[selected_ipdu]:
        updated_config_data_table =[
        html.Tr([
            html.Td(strs.common_name[feed]),
            html.Td(dcc.Input(id=f'{feed}_SNAPSHOT_INPUT', type='number', inputMode='numeric', value=cfg.config_data[selected_ipdu][f'{feed}_SNAPSHOT'], disabled=cfg.config_data[selected_ipdu][f'{feed}_SNAPSHOT'] == 'N/A', className='bg-secondary text-dark config-td text-right')),
            html.Td(dcc.Input(id=f'{feed}_REALTIME_INPUT', type='number', inputMode='numeric', value=cfg.config_data[selected_ipdu][f'{feed}_REALTIME'], disabled=cfg.config_data[selected_ipdu][f'{feed}_REALTIME'] == 'N/A', className='bg-secondary text-dark config-td text-right')),
            html.Td(dcc.Input(id=f'{feed}_MAX_INPUT', type='number', inputMode='numeric', value=cfg.config_data[selected_ipdu][f'{feed}_MAX'], disabled=cfg.config_data[selected_ipdu][f'{feed}_MAX'] == 'N/A', className='bg-secondary text-dark config-td text-right')),
            html.Td(dcc.Input(id=f'{feed}_MIN_INPUT', type='number', inputMode='numeric', value=cfg.config_data[selected_ipdu][f'{feed}_MIN'], disabled=cfg.config_data[selected_ipdu][f'{feed}_MIN'] == 'N/A', className='bg-secondary text-dark config-td text-right')),
        ]) for feed in strs.feeds]
        updated_config_data_table.insert(0,
            html.Tr([
            html.Th('Feed', className='text-dark'),
            html.Th('Snapshot', className='text-dark'),
            html.Th('Realtime', className='text-dark'),
            html.Th('High Alarm', className='text-dark'),
            html.Th('Low Alarm', className='text-dark')
        ]))

        updated_config_oversubscription_table = [
        html.Tr([
            html.Td(circuit),
            html.Td(dcc.Input(id=f'OVER_SUB_{circuit}_INPUT', type='number', inputMode='numeric', value=cfg.config_data[selected_ipdu][f'OVER_SUB_{circuit}_AMP'], disabled=cfg.config_data[selected_ipdu][f'OVER_SUB_{circuit}_AMP'] == 'N/A', className='bg-secondary text-dark config-td float-right text-right'))
        ]) for circuit in ['MAIN'] + strs.circuits]
        
        
        bit_BYP = 1 << 0
        bit_20B = 1 << 1 
        bit_20A = 1 << 2
        bit_40B = 1 << 3 
        bit_40A = 1 << 4 
        bit_60B = 1 << 5
        bit_60A = 1 << 6

        manual_shed_val = {
            '60A': cfg.config_data[selected_ipdu]['feedControlState'] & bit_60A != 0,
            '60B': cfg.config_data[selected_ipdu]['feedControlState'] & bit_60B != 0,
            '40A': cfg.config_data[selected_ipdu]['feedControlState'] & bit_40A != 0,
            '40B': cfg.config_data[selected_ipdu]['feedControlState'] & bit_40B != 0,
            '20A': cfg.config_data[selected_ipdu]['feedControlState'] & bit_20A != 0,
            '20B': cfg.config_data[selected_ipdu]['feedControlState'] & bit_20B != 0,
            'BYP': cfg.config_data[selected_ipdu]['feedControlState'] & bit_BYP != 0
        }


        updated_config_priority_table = [    
            html.Tr(className='alarm-header',children=[
                html.Th(className='alarm-th', children=[
                    html.Span(children='Circuit', className='text-dark')
                ]),
                html.Th(className='alarm-th', children=[
                    html.Span(children='Priority', className='text-dark')
                ]),
                html.Th(className='alarm-th', children=[
                    html.Span(className='text-warning',children='Manual Shed')
                ]),
            ]),
            *[html.Tr([
                html.Td(circuit),
                html.Td(dcc.Input(id=f'CONFIG_{circuit}_PRIORITY', type='number', inputMode='numeric', value=cfg.config_data[selected_ipdu][f'MAIN_{circuit}_PRIORITY'], disabled=False, className='bg-secondary text-dark config-td float-right text-right')),
                html.Td(className='alarm-td', children=[daq.BooleanSwitch(id=f'CONFIG_{circuit}_manual_shed', on=manual_shed_val[circuit], color=theme_colors[theme]['warning'])])
            ]) for circuit in strs.circuits],
            html.Tr([
                html.Td('TEST'),
                html.Td(),
                html.Td(className='alarm-td', children=[daq.BooleanSwitch(id='CONFIG_BYP_manual_shed', on=manual_shed_val['BYP'], color=theme_colors[theme]['warning'])])
            ])  ]

        updated_led_state = 'On' if cfg.config_data[selected_ipdu]['ledState'] else 'Off'
        updated_freq_state = '50' if cfg.config_data[selected_ipdu]['inputFrquency'] else '60'
        

        return html.Table(updated_config_data_table), [html.Div('Oversubscription Values', className='small-header text-dark'), html.Table(updated_config_oversubscription_table, className='w-100')], [html.Div('Priorities', className='small-header text-dark'), html.Table(updated_config_priority_table, className='w-100')], updated_led_state, updated_freq_state
    
    return html.Table(config_data_table), [html.Div('Oversubscription Values', className='small-header text-dark'), html.Table(config_oversubscription_table, className='w-100')], [html.Div('Priorities', className='small-header text-dark'), html.Table(config_priority_table, className='w-100')], 'On', '60'

#For submitting configuration data
@app.callback(
    Output('config-dummy', 'children'),
    Input('config-submit-btn', 'n_clicks'),
    State('led_select_radio_items', 'value'),
    State('freq_select_radio_items', 'value'),
    State('CONFIG_60A_manual_shed', 'on'),
    State('CONFIG_60B_manual_shed', 'on'),
    State('CONFIG_40A_manual_shed', 'on'),
    State('CONFIG_40B_manual_shed', 'on'),
    State('CONFIG_20A_manual_shed', 'on'),
    State('CONFIG_20B_manual_shed', 'on'),
    State('CONFIG_BYP_manual_shed', 'on'),
    *config_data_inputs)
def submit_config(button, led_value, freq_val, val_60A, val_60B, val_40A, val_40B, val_20A, val_20B, val_BYP, *args):
    if button and selected_ipdu in cfg.config_data and cfg.config_data[selected_ipdu] and args[0] is not None:
        bit_BYP = 1 << 0 if val_BYP is not None and val_BYP else 0
        bit_20B = 1 << 1 if val_20B is not None and val_20B else 0
        bit_20A = 1 << 2 if val_20A is not None and val_20A else 0
        bit_40B = 1 << 3 if val_40B is not None and val_40B else 0
        bit_40A = 1 << 4 if val_40A is not None and val_40A else 0
        bit_60B = 1 << 5 if val_60B is not None and val_60B else 0
        bit_60A = 1 << 6 if val_60A is not None and val_60A else 0
        
        config_list = [
            args[74],      #MAIN_L1_VOLT_MAX
            args[111],     #MAIN_L1_VOLT_MIN
            args[75],      #MAIN_L2_VOLT_MAX
            args[112],     #MAIN_L2_VOLT_MIN
            args[76],      #MAIN_L3_VOLT_MAX
            args[113],     #MAIN_L3_VOLT_MIN
            
            args[77],      #MAIN_FREQ_MAX 
            args[114],     #MAIN_FREQ_MIN
            args[80],      #MAIN_PHASE_MAX 
            args[117],     #MAIN_PHASE_MIN

            args[83],                                # MAIN_L1_AMP_MAX 
            args[84],                                # MAIN_L2_AMP_MAX 
            args[85],                                # MAIN_L3_AMP_MAX 
            args[86],                                # MAIN_N_AMP_MAX 
                                            #  
            args[87],                                # F60A_L1_AMP_MAX 
            args[88],                                # F60A_L2_AMP_MAX 
            args[89],                                # F60A_L3_AMP_MAX 
            args[90],                                # F60A_N_AMP_MAX 
                                            #  
            args[91],                                # F60B_L1_AMP_MAX 
            args[92],                                # F60B_L2_AMP_MAX 
            args[93],                                # F60B_L3_AMP_MAX 
            args[94],                                # F60B_N_AMP_MAX 
                                            #  
            args[95],                                # F40A_L1_AMP_MAX 
            args[96],                                # F40A_L2_AMP_MAX 
            args[97],                                # F40A_L3_AMP_MAX 
            args[98],                                # F40A_N_AMP_MAX 
                                            #  
            args[99],                                # F40B_L1_AMP_MAX 
            args[100],                                # F40B_L2_AMP_MAX 
            args[101],                                # F40B_L3_AMP_MAX 
            args[102],                                # F40B_N_AMP_MAX 
                                            #  
            args[103],                                # F20A_L1_AMP_MAX 
            args[104],                                # F20A_N_AMP_MAX 
            args[105],                                # F20B_L2_AMP_MAX 
            args[106],                                # F20B_N_AMP_MAX 
                                            #  
            args[107],                               # FBYP_L1_AMP_MAX 
            args[108],                                # FBYP_L2_AMP_MAX 
            args[109],                                # FBYP_L3_AMP_MAX 
            args[110],                                # FBYP_N_AMP_MAX 

            args[148],                                # OVER_SUB_MAIN_AMP 
            args[149],                                # OVER_SUB_60A_AMP 
            args[150],                                # OVER_SUB_60B_AMP 
            args[151],                                # OVER_SUB_40A_AMP 
            args[152],                                # OVER_SUB_40B_AMP 
            args[153],                                # OVER_SUB_20A_AMP 
            args[154],                                # OVER_SUB_20B_AMP 

            args[155],                                      #MAIN_60A_PRIORITY
            args[156],                                      #MAIN_60B_PRIORITY
            args[157],                                      #MAIN_40A_PRIORITY
            args[158],                                      #MAIN_40B_PRIORITY
            args[159],                                      #MAIN_20A_PRIORITY
            args[160],                                      #MAIN_20B_PRIORITY
                                            #  
            
            0 if freq_val == '60' else 1,                                      #INPUT FREQUENCY
            1 if led_value == 'On' else 0,                                      #LED STATE
            bit_BYP | bit_20A | bit_20B | bit_40A | bit_40B | bit_60A | bit_60B,                                      #FEED CONTROL STATE
                                            #           This block of snapshot correction values are floating point doubles 
            args[0],                                # VOLTAGE_FACTOR_L1 
            args[1],                                # VOLTAGE_FACTOR_L2 
            args[2],                                # VOLTAGE_FACTOR_L3 
                                            #  
            args[9],                                # CURRENT_MAIN_FACTOR_L1 
            args[10],                                # CURRENT_MAIN_FACTOR_L2 
            args[11],                                # CURRENT_MAIN_FACTOR_L3 
            args[12],                                # CURRENT_MAIN_FACTOR_N 
                                            #  
            args[13],                                # CURRENT_60A_FACTOR_L1 
            args[14],                                # CURRENT_60A_FACTOR_L2 
            args[15],                                # CURRENT_60A_FACTOR_L3 
            args[16],                                # CURRENT_60A_FACTOR_N 
                                            #  
            args[17],                                # CURRENT_60B_FACTOR_L1 
            args[18],                                # CURRENT_60B_FACTOR_L2 
            args[19],                                # CURRENT_60B_FACTOR_L3 
            args[20],                                # CURRENT_60B_FACTOR_N 
                                            #  
            args[21],                                # CURRENT_40A_FACTOR_L1 
            args[22],                                # CURRENT_40A_FACTOR_L2 
            args[23],                                # CURRENT_40A_FACTOR_L3 
            args[24],                                # CURRENT_40A_FACTOR_N 
                                            #  
            args[25],                                # CURRENT_40B_FACTOR_L1 
            args[26],                                # CURRENT_40B_FACTOR_L2 
            args[27],                                # CURRENT_40B_FACTOR_L3 
            args[28],                                # CURRENT_40B_FACTOR_N 
                                            #  
            args[29],                                # CURRENT_20A_FACTOR_L1 
            args[30],                                # CURRENT_20A_FACTOR_N 
                                            #  
            args[31],                                # CURRENT_20B_FACTOR_L1 
            args[32],                                # CURRENT_20B_FACTOR_N 
                                            #  
            args[33],                                # CURRENT_BYPASS_FACTOR_L1 
            args[34],                                # CURRENT_BYPASS_FACTOR_L2 
            args[35],                                # CURRENT_BYPASS_FACTOR_L3 
            args[36],                                # CURRENT_BYPASS_FACTOR_N 
                                            #  
                                            #  
                                            #           This block of real-time correction values are floating point doubles 
            args[37],                                # VOLTAGE_L1 
            args[38],                                # VOLTAGE_L2 
            args[39],                                # VOLTAGE_L3 
            cfg.config_data[selected_ipdu]['noise'],                                # NOISE 
                                            #  
            args[46],                                # MAIN_L1_AMP 
            args[47],                                # MAIN_L2_AMP 
            args[48],                                # MAIN_L3_AMP 
            args[49],                                # MAIN_N_AMP 
                                            #  
            args[50],                                # F60A_L1_AMP 
            args[51],                                # F60A_L2_AMP 
            args[52],                                # F60A_L3_AMP 
            args[53],                                # F60A_N_AMP 
                                            #  
            args[54],                                # F60B_L1_AMP 
            args[55],                                # F60B_L2_AMP 
            args[56],                                # F60B_L3_AMP 
            args[57],                                # F60B_N_AMP 
                                            #  
            args[58],                                # F40A_L1_AMP 
            args[59],                                # F40A_L2_AMP 
            args[60],                                # F40A_L3_AMP 
            args[61],                                # F40A_N_AMP 
                                            #  
            args[62],                                # F40B_L1_AMP 
            args[63],                                # F40B_L2_AMP 
            args[64],                                # F40B_L3_AMP 
            args[65],                                # F40B_N_AMP 
                                            #  
            args[66],                                # F20A_L1_AMP 
            args[67],                                # F20A_N_AMP 
                                            #  
            args[68],                                # F20B_L2_AMP 
            args[69],                                # F20B_N_AMP 
                                            #  
            args[70],                                # FBYP_L1_AMP 
            args[71],                                # FBYP_L2_AMP 
            args[72],                                # FBYP_L3_AMP 
            args[73]                                # FBYP_N_AMP
        ]
        config_list = [int(i) for i in config_list[:54]]+[float(d) for d in config_list[54:117]]
        message_struct = struct.Struct('54I63d')
        message_data = message_struct.pack(*config_list)
        gc.send_message(b'SC', message_data)
        strs.event_queue.append((time.ctime()[11:19], 'Submitted Configuration.', 'info'))
    return ''

@app.callback(
    Output('init-priority-dummy', 'className'),
    Input('ipdu-dropdown', 'value'),
    Input('update_interval', 'n_intervals'))
def update_ipdu_info(ipdu, interval):
    ctx = dash.callback_context
    if ctx.triggered:
        triggered_input = ctx.triggered[0]['prop_id'].split('.')[0]
        if triggered_input == 'update_interval':
            if mp.ipdu_temperature is None and selected_ipdu:
                gc.send_message(b'GT')
                gc.send_message(b'GV')
            
        elif ipdu and ipdu < len(strs.ipdus) and selected_ipdu:
            gc.send_message(b'GT')
            gc.send_message(b'GV')
    return ''

@app.callback(
    Output('60A_priority_input', 'value'),
    Output('60B_priority_input', 'value'),
    Output('40A_priority_input', 'value'),
    Output('40B_priority_input', 'value'),
    Output('20A_priority_input', 'value'),
    Output('20B_priority_input', 'value'),
    Input('refresh-feed-control-btn', 'n_clicks'),
    Input('ipdu-dropdown', 'value'))
def refresh_priorities(button, ipdu):
    if ipdu is not None and ipdu < len(strs.ipdus):
        cfg.read(strs.ipdus[ipdu])

    if ipdu is not None and ipdu < len(strs.ipdus) and strs.ipdus[ipdu] in cfg.config_data and cfg.config_data[strs.ipdus[ipdu]]:
        return tuple([cfg.config_data[strs.ipdus[ipdu]][f'MAIN_{circuit}_PRIORITY'] for circuit in strs.circuits])
    return None, None, None, None, None, None

@app.callback(
    Output('submit-dummy', 'children'),
    Input('submit-btn', 'n_clicks'),
    State('60A_manual_shed', 'on'),
    State('60B_manual_shed', 'on'),
    State('40A_manual_shed', 'on'),
    State('40B_manual_shed', 'on'),
    State('20A_manual_shed', 'on'),
    State('20B_manual_shed', 'on'),
    State('BYP_manual_shed', 'on'),
    State('60A_priority_input', 'value'),
    State('60B_priority_input', 'value'),
    State('40A_priority_input', 'value'),
    State('40B_priority_input', 'value'),
    State('20A_priority_input', 'value'),
    State('20B_priority_input', 'value'))
def submit_feed_control(button, val_60A, val_60B, val_40A, val_40B, val_20A, val_20B, val_BYP, priority_60A, priority_60B, priority_40A, priority_40B, priority_20A, priority_20B):
    if selected_ipdu and button:
        bit_BYP = 1 << 0 if val_BYP is not None and val_BYP else 0
        bit_20B = 1 << 1 if val_20B is not None and val_20B else 0
        bit_20A = 1 << 2 if val_20A is not None and val_20A else 0
        bit_40B = 1 << 3 if val_40B is not None and val_40B else 0
        bit_40A = 1 << 4 if val_40A is not None and val_40A else 0
        bit_60B = 1 << 5 if val_60B is not None and val_60B else 0
        bit_60A = 1 << 6 if val_60A is not None and val_60A else 0

        priority_60A = max(min(priority_60A, 255), 0) if priority_60A is not None else cfg.config_data[selected_ipdu]['MAIN_60A_PRIORITY']
        priority_60B = max(min(priority_60B, 255), 0) if priority_60B is not None else cfg.config_data[selected_ipdu]['MAIN_60B_PRIORITY']
        priority_40A = max(min(priority_40A, 255), 0) if priority_40A is not None else cfg.config_data[selected_ipdu]['MAIN_40A_PRIORITY']
        priority_40B = max(min(priority_40B, 255), 0) if priority_40B is not None else cfg.config_data[selected_ipdu]['MAIN_40B_PRIORITY']
        priority_20A = max(min(priority_20A, 255), 0) if priority_20A is not None else cfg.config_data[selected_ipdu]['MAIN_20A_PRIORITY']
        priority_20B = max(min(priority_20B, 255), 0) if priority_20B is not None else cfg.config_data[selected_ipdu]['MAIN_20B_PRIORITY']
        
        feed_control_data = bit_BYP | bit_20A | bit_20B | bit_40A | bit_40B | bit_60A | bit_60B
        gc.send_message(b'SP', feed_control_data.to_bytes(4, 'little'))
        feed_priority_data = priority_60A.to_bytes(4, 'little') + priority_60B.to_bytes(4, 'little') + priority_40A.to_bytes(4, 'little') + priority_40B.to_bytes(4, 'little') + priority_20A.to_bytes(4, 'little') + priority_20B.to_bytes(4, 'little')
        gc.send_message(b'SD', feed_priority_data)
        
        strs.event_queue.append((time.ctime()[11:19], 'Submitted Feed Control.', 'info'))
    return ''

#For selecting multi-use display area tab
@app.callback(
    Output('waveform', 'style'),
    Output('snapshot', 'style'),
    Input('display-tabs', 'value'))
def render_content(tab):
    if tab == 'waveform':
        return {'display':'block', 'height':'600px'}, {'display':'none'}
    elif tab == 'snapshot':
        return {'display':'none'}, {'display':'block', 'height':'600px'}

@app.callback(
    Output('main-voltage-graph', 'figure'),
    Input('update_interval','n_intervals'))
def update_main_voltage_graph(interval):
    if selected_ipdu in mp.snapshot_history and mp.snapshot_history[selected_ipdu]:
        voltage_fig = go.Figure(
            data=[{'y': mp.snapshot_history[selected_ipdu][key], 'name': strs.common_name[key], 'mode':'lines+markers'}  for key in ['MAIN_L1_VOLT', 'MAIN_L2_VOLT', 'MAIN_L3_VOLT']],
            layout=main_snapshot_layout
        )
        traces = [mp.snapshot_history[selected_ipdu][key] for key in ['MAIN_L1_VOLT', 'MAIN_L2_VOLT', 'MAIN_L3_VOLT']]
        yvals = []
        for trace in traces:
            yvals += trace
        
        voltage_fig.layout.yaxis = {'fixedrange': True, 'range': [min(yvals)*.95, max(yvals+[1])*1.05 ]}
        
        #Add shaded areas to show oversubscription and max current values.
        if selected_ipdu in cfg.config_data and cfg.config_data[selected_ipdu] is not None and len(cfg.config_data[selected_ipdu]) > 0 and f'OVER_SUB_MAIN_AMP' in cfg.config_data[selected_ipdu].keys():
            min_volt_max = min(cfg.config_data[selected_ipdu][f'MAIN_{line}_VOLT_MAX'] for line in strs.get_lines['MAIN'][:-1])
            max_volt_max = min_volt_max*5
            voltage_fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[min_volt_max, min_volt_max], fill='none', mode='lines', line=dict(width=0), showlegend=False))
            voltage_fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[max_volt_max, max_volt_max], fill='tonexty', mode='lines', line_color=theme_colors[theme]['danger'], line=dict(width=0), showlegend=False))
            
            max_volt_min = max(cfg.config_data[selected_ipdu][f'MAIN_{line}_VOLT_MIN'] for line in strs.get_lines['MAIN'][:-1])
            voltage_fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[max_volt_min, max_volt_min], fill='tozeroy', mode='lines', line_color=theme_colors[theme]['danger'], line=dict(width=0), showlegend=False))
            
            # voltage_fig.layout = main_snapshot_layout
            # voltage_fig.layout.yaxis = {'fixedrange': True, 'range': [0, max_volt_max]}
        return voltage_fig
    return {'layout': main_snapshot_layout}

@app.callback(
    Output('main-current-graph', 'figure'),
    Input('update_interval','n_intervals'))
def update_main_current_graph(interval):
    if selected_ipdu in mp.snapshot_history and mp.snapshot_history[selected_ipdu]:
        current_fig = go.Figure(
            data=[{'y': mp.snapshot_history[selected_ipdu][key], 'name': strs.common_name[key], 'mode':'lines+markers'}  for key in ('MAIN_L1_AMP', 'MAIN_L2_AMP', 'MAIN_L3_AMP', 'MAIN_N_AMP')],
            layout=main_snapshot_layout
        )
        traces = [mp.snapshot_history[selected_ipdu][key] for key in ['MAIN_L1_AMP', 'MAIN_L2_AMP', 'MAIN_L3_AMP', 'MAIN_N_AMP']]
        yvals = [1]
        for trace in traces:
            yvals += trace
        
        current_fig.layout.yaxis = {'fixedrange': True, 'range': [0, max(yvals)*1.2]}

        if selected_ipdu in cfg.config_data and cfg.config_data[selected_ipdu] is not None and len(cfg.config_data[selected_ipdu]) > 0 and f'OVER_SUB_MAIN_AMP' in cfg.config_data[selected_ipdu].keys():
                min_amp_max = min(cfg.config_data[selected_ipdu][f'MAIN_{line}_AMP_MAX'] for line in strs.get_lines['MAIN'])
                max_amp_max = min_amp_max*5
                current_fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[cfg.config_data[selected_ipdu][f'OVER_SUB_MAIN_AMP']]*2, fill='none', mode='lines', line=dict(width=0), showlegend=False))
                current_fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[min_amp_max, min_amp_max], fill='tonexty', mode='lines', line_color=theme_colors[theme]['warning'], line=dict(width=0), showlegend=False))
                current_fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[max_amp_max, max_amp_max], fill='tonexty', mode='lines', line_color=theme_colors[theme]['danger'], line=dict(width=0), showlegend=False))
                
                # current_fig.layout = main_snapshot_layout
                # current_fig.layout.yaxis = {'fixedrange': True, 'range': [0, max_amp_max]}
        return current_fig
    return {'layout': main_snapshot_layout}

@app.callback(
    Output('main-info-table', 'children'),
    Input('update_interval','n_intervals'))
def update_main_info_table(interval):
    if selected_ipdu in mp.snapshot_history and mp.snapshot_history[selected_ipdu]:
        info_table = [
            html.Table(className='w-100', children=[
                html.Tr(children=[
                    html.Td('L1 Volt'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L1_VOLT'],2), className='text-right')
                ]),
                html.Tr(children=[
                    html.Td('L2 Volt'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L2_VOLT'],2), className='text-right')
                ]),
                html.Tr(children=[
                    html.Td('L3 Volt'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L3_VOLT'],2), className='text-right')
                ]),
            ]),
            html.Hr(),
            html.Table(className='w-100', children=[
                *[html.Tr([html.Td(f'{line} Amp'), html.Td(round(mp.snapshot_data[selected_ipdu][f'MAIN_{line}_AMP'],2), className='text-right')]) for line in strs.get_lines['MAIN']]
            ]),
            html.Hr(),
            html.Table(className='w-100', children=[
                html.Tr(children=[
                    html.Td('L1 Freq'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L1_FREQ'],2), className='text-right')
                ]),
                html.Tr(children=[
                    html.Td('L2 Freq'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L2_FREQ'],2), className='text-right')
                ]),
                html.Tr(children=[
                    html.Td('L3 Freq'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L3_FREQ'],2), className='text-right')
                ]),
                html.Tr(children=[
                    html.Td('L1-L2 Phase'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L1_L2_PHASE'],2), className='text-right')
                ]),
                html.Tr(children=[
                    html.Td('L2-L3 Phase'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L2_L3_PHASE'],2), className='text-right')
                ]),
                html.Tr(children=[
                    html.Td('L3-L1 Phase'),
                    html.Td(round(mp.snapshot_data[selected_ipdu]['MAIN_L3_L1_PHASE'],2), className='text-right')
                ])
            ])
        ]
        return info_table
    #if no data available, return empty layout
    return ['No data']

@app.callback(
    *breaker_indicators_value_outputs,
    *contactor_indicators_value_outputs,
    *breaker_indicators_color_outputs,
    *contactor_indicators_color_outputs,
    Input('update_interval','n_intervals'))
def update_indicators(interval):
    if selected_ipdu in mp.snapshot_data and mp.snapshot_data[selected_ipdu]:
        return tuple(
                [mp.snapshot_data[selected_ipdu][f'{circuit}_breaker'] for circuit in strs.circuits + ['MAIN']] + \
                [mp.snapshot_data[selected_ipdu][f'{circuit}_contactor'] for circuit in strs.circuits + ['BYP']] +\
                
                [(theme_colors[theme]['danger'] if mp.snapshot_data[selected_ipdu][f'{circuit}_breaker'] else '#AAA') for circuit in strs.circuits + ['MAIN']] + \
                [(theme_colors[theme]['warning'] if mp.snapshot_data[selected_ipdu][f'{circuit}_contactor'] else '#AAA') for circuit in strs.circuits + ['BYP']])

    return tuple([False for i in strs.circuits + ['MAIN']] + [False for i in strs.circuits + ['BYP']] + ['#AAA' for i in strs.circuits + ['MAIN']] + ['#AAA' for i in strs.circuits + ['BYP']])

#Updates the alarm modal body
@app.callback(
    Output('alarm-body', 'children'),
    Input('update_interval', 'n_intervals'))
def update_alarm_data(interval):
    result = html.Div(children=[])
    for ipdu in mp.alarms:
        table_rows = []
        if mp.alarms[ipdu] and mp.alarm_count[ipdu] > 0:
            result.children.append(ipdu)
            for circuit in mp.alarms[ipdu]:
                for alarm in mp.alarms[ipdu][circuit]:
                    table_rows.append(html.Tr([html.Td(strs.common_name[circuit], className='p-1'), html.Td(f'{mp.alarms[ipdu][circuit][alarm]}', className='p-1 text-right')]))
            result.children.append(html.Table(table_rows, className='m-auto w-75'))
            result.children.append(html.Hr())
    #Remove last <hr>
    if result.children:
        result.children.pop()

        return result
            
    return 'No Alarms'

@app.callback(
    Output('event-history-modal-body', 'children'),
    Input('update_interval', 'n_intervals'))
def update_event_history_modal_body(interval):
    if not event_history:
        return 'No Events'
    return html.Table(
        [html.Tr(html.Td(event)) for event in event_history]
    )

@app.callback(
    Output('message-box', 'children'),
    Input('update_interval', 'n_intervals'),
    Input('message-box', 'children'))
def update_message_box(interval, in_children):
    result = []
    global event_history

    # while mp.alarm_message_queue:
    #     msg = mp.alarm_message_queue.pop()
    #     result.append(dbc.Alert(msg, is_open=True, dismissable=True, fade=True, color='danger', className='5 w-100', style={'height':'50px', 'position': 'absolute', 'bottom':f'{len(result)*50}px'}))
    #     event_history.append(msg)
    # while rtd_message_queue:
    #     msg = rtd_message_queue.pop()
    #     result.append(dbc.Alert(msg, is_open=True, dismissable=True, fade=True, color='success', className='5 w-100', style={'height':'50px', 'position': 'absolute', 'bottom':f'{len(result)*50}px'}))
    #     event_history.append(msg)

    while strs.event_queue:
        time, msg, color = strs.event_queue.pop()
        result.append(dbc.Alert(msg, is_open=True, dismissable=True, fade=True, color=color, className='5 w-100 text-dark', style={'height':'50px', 'position': 'absolute', 'bottom':f'{len(result)*50}px'}))
        event_history.append(f'{time} ' + msg)

    for child in in_children:
        if child and 'props' in child and 'is_open' in child['props'] and child['props']['is_open']:
            duration = int(child['props']['className'].split(' ')[0])
            if duration > 0:
                result.append(dbc.Alert(str(child['props']['children']), is_open=True, dismissable=True, fade=True, color = child['props']['color'], className=f'{duration-1} w-100 text-dark', style={'height':'50px', 'position': 'absolute', 'bottom':f'{len(result)*50}px'}))
            else:
                result.append(dbc.Alert(str(child['props']['children']), is_open=False, dismissable=True, fade=True, color = child['props']['color'], className='0 w-100 text-dark', style={'height':'50px', 'position': 'absolute', 'bottom':f'{len(result)*50}px'}))
    

    if len(event_history) > 50:
        event_history = event_history[-50:]

    return result
#Graphs the selected snapshot data
@app.callback(
    Output('snapshot_select_gauges_title', 'children'),
    Output('snapshot-graph', 'figure'),
    Input('snapshot_select_radio_items', 'value'),
    Input('update_interval', 'n_intervals'))
def update_snapshot_display(selected_value, interval):
    if selected_ipdu in mp.snapshot_data and mp.snapshot_data[selected_ipdu]:
        fig = go.Figure(
            data=[{'y': mp.snapshot_history[selected_ipdu][key], 'name': strs.common_name[key], 'mode':'lines+markers'}  for key in [f'{selected_value}_{line}_AMP' for line in strs.get_lines[selected_value]]],
            layout=snapshot_select_layout
        )
        traces = [mp.snapshot_history[selected_ipdu][key] for key in [f'{selected_value}_{line}_AMP' for line in strs.get_lines[selected_value]]]
        yvals = [1]
        for trace in traces:
            yvals += trace
        
        fig.layout.yaxis = {'fixedrange': True, 'range': [0, max(yvals)*1.2]}
        
        #Add shaded areas to show oversubscription and max current values.
        if selected_ipdu in cfg.config_data and cfg.config_data[selected_ipdu] is not None and len(cfg.config_data[selected_ipdu]) > 0 and f'OVER_SUB_{selected_value}_AMP' in cfg.config_data[selected_ipdu].keys():
            min_amp_max = min(cfg.config_data[selected_ipdu][f'{selected_value}_{line}_AMP_MAX'] for line in strs.get_lines[selected_value])
            max_amp_max = min_amp_max*5 #1.3
            fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[cfg.config_data[selected_ipdu][f'OVER_SUB_{selected_value}_AMP']]*2, fill='none', mode='lines', line=dict(width=0), showlegend=False))
            fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[min_amp_max, min_amp_max], fill='tonexty', mode='lines', line_color=theme_colors[theme]['warning'], line=dict(width=0), showlegend=False))
            fig.add_trace(go.Scatter(x=[0, max_snapshot_history], y=[max_amp_max, max_amp_max], fill='tonexty', mode='lines', line_color=theme_colors[theme]['danger'], line=dict(width=0), showlegend=False))
            


        return selected_value, fig
    return selected_value, {'layout': snapshot_select_layout}

@app.callback(
    Output('snapshot-select-powerfactor-table', 'children'),
    Input('snapshot_select_radio_items', 'value'),
    Input('update_interval', 'n_intervals'))
def update_snapshot_info_powerfactors(selected_feed, interval):
    if selected_ipdu in mp.snapshot_data and mp.snapshot_data[selected_ipdu] and selected_feed is not None:
        return list( [html.Tr([html.Td(f'{line} PF'), html.Td(round(mp.snapshot_data[selected_ipdu][f'PF_{selected_feed}_{line}'], 2), className='text-right')]) for line in strs.get_lines[selected_feed][:-1]]) 
    return 'No Data'

@app.callback(
    Output('snapshot-select-info-table', 'children'),
    Input('snapshot_select_radio_items', 'value'),
    Input('update_interval', 'n_intervals'))
def update_snapshot_info_amps(selected_feed, interval):
    if selected_ipdu in mp.snapshot_data and mp.snapshot_data[selected_ipdu] and selected_feed is not None:
        return list( [html.Tr([html.Td(f'{line} Amp'), html.Td(round(mp.snapshot_data[selected_ipdu][f'{selected_feed}_{line}_AMP'],2), className='text-right')]) for line in strs.get_lines[selected_feed]]) 
    return 'No Data'

#Graphs the selected waveforms
@app.callback(
    Output('waveform-graph', 'figure'),
    Output('waveform-rms-panel-content', 'children'),
    Output('rtd-filename', 'children'),
    Output('waveform-select-60A', 'style'),
    Output('waveform-select-60B', 'style'),
    Output('waveform-select-40A', 'style'),
    Output('waveform-select-40B', 'style'),
    Output('waveform-select-20A', 'style'),
    Output('waveform-select-20B', 'style'),
    Output('waveform-select-MAIN', 'style'),
    Output('waveform-select-BYP', 'style'),
    Output('waveform-select-VOLT', 'style'),
    Input('waveform-select-60A', 'value'),
    Input('waveform-select-60B', 'value'),
    Input('waveform-select-40A', 'value'),
    Input('waveform-select-40B', 'value'),
    Input('waveform-select-20A', 'value'),
    Input('waveform-select-20B', 'value'),
    Input('waveform-select-MAIN', 'value'),
    Input('waveform-select-BYP', 'value'),
    Input('waveform-select-VOLT', 'value'),
    Input('waveform-select-dropdown', 'value'))
def update_waveform_display(val_60A, val_60B, val_40A, val_40B, val_20A, val_20B, val_MAIN, val_BYP, val_VOLT, filename):
    global prev_filename
    values = val_60A + val_60B + val_40A + val_40B + val_20A + val_20B + val_MAIN + val_BYP + val_VOLT
    
    style_60A = {'background-color': theme_colors[theme]['secondary']}
    style_60B = {'background-color': theme_colors[theme]['secondary']}
    style_40A = {'background-color': theme_colors[theme]['secondary']}
    style_40B = {'background-color': theme_colors[theme]['secondary']}
    style_20A = {'background-color': theme_colors[theme]['secondary']}
    style_20B = {'background-color': theme_colors[theme]['secondary']}
    style_MAIN = {'background-color': theme_colors[theme]['secondary']}
    style_BYP = {'background-color': theme_colors[theme]['secondary']}
    style_VOLT = {'background-color': theme_colors[theme]['secondary']}

    waveform_fig = go.Figure(layout=waveform_layout)
    
    if selected_ipdu is not None and selected_ipdu > 0:
        #Highlight circuits involved in the realtime event
        if filename is not None:
            events = rtd.involved_events(data_path + str(selected_ipdu) + '/' + filename)
            for event in events:
                event = event.strip()
                if event in ['F60A_L1_AMP', 'F60A_L2_AMP', 'F60A_L3_AMP', 'F60A_N_AMP']:
                    style_60A = {'background': theme_colors[theme]['warning']}
                if event in ['F60B_L1_AMP', 'F60B_L2_AMP', 'F60B_L3_AMP', 'F60B_N_AMP']:
                    style_60B = {'background': theme_colors[theme]['warning']}
                if event in ['F40A_L1_AMP', 'F40A_L2_AMP', 'F40A_L3_AMP', 'F40A_N_AMP']:
                    style_40A = {'background': theme_colors[theme]['warning']}
                if event in ['F40B_L1_AMP', 'F40B_L2_AMP', 'F40B_L3_AMP', 'F40B_N_AMP']:
                    style_40B = {'background': theme_colors[theme]['warning']}
                if event in ['F20A_L1_AMP', 'F20A_N_AMP']:
                    style_20A = {'background': theme_colors[theme]['warning']}
                if event in ['F20B_L2_AMP', 'F20B_N_AMP']:
                    style_20B = {'background': theme_colors[theme]['warning']}
                if event in ['MAIN_L1_AMP', 'MAIN_L2_AMP', 'MAIN_L3_AMP', 'MAIN_N_AMP']:
                    style_MAIN = {'background': theme_colors[theme]['warning']}
                if event in ['BYP_L1_AMP', 'BYP_L2_AMP', 'BYP_L3_AMP', 'BYP_N_AMP']:
                    style_BYP = {'background': theme_colors[theme]['warning']}
                if event in ['MAIN_L1_VOLT', 'MAIN_L2_VOLT', 'MAIN_L3_VOLT']:
                    style_VOLT = {'background': theme_colors[theme]['warning']}

        if filename != prev_filename and filename is not None:
            prev_filename = filename
            rtd.read(data_path + str(selected_ipdu) + '/' + filename)

        if rtd.realtime_data is not None and len(rtd.realtime_data) > 0 and values is not None and len(values) > 0 and filename is not None:
            for val in values:
                waveform_fig.add_trace(go.Scatter(x= (rtd.realtime_data['TIME_LOW'] - min(rtd.realtime_data['TIME_LOW'])) + (rtd.realtime_data['TIME_NANO'] * 0.000000001), y= rtd.realtime_data[f'{val}'], name=val))
            
            return waveform_fig, html.Table(className='w-100', children=[
                html.Tr(children=[html.Td(f'{strs.common_name[val]}:', className='p-1'), html.Td(rtd.rms[f'{val}'], className='p-1 text-right')]) for val in values
            ]), filename, style_60A, style_60B, style_40A, style_40B, style_20A, style_20B, style_MAIN, style_BYP, style_VOLT

    #if no figure selected / available, return nothing
    return waveform_fig, 'Select waveforms to view RMS values.', 'No File Selected' if filename is None else filename, style_60A, style_60B, style_40A, style_40B, style_20A, style_20B, style_MAIN, style_BYP, style_VOLT

#Loads correct page based on URL (e.g. /config)
@app.callback(
    Output('page-content', 'children'),
    Input('url', 'pathname'))
    # Input('dark-mode-toggle', 'on'))
def display_page(pathname):
    if pathname == '/config':
        return config_layout
    else:
        return main_layout

#Updates the connection status
@app.callback(
    Output('waiting_connection','children'),
    Input('update_interval','n_intervals'))
def update_connection_status(val):
    if gc.is_connected():
        return html.Span(f'IPDU: {selected_ipdu}', className='title text-dark')
    return [html.Span('Connecting to controller ', className='title text-dark'), dbc.Spinner(color='info')]

@app.callback(
    Output('waveform-select-dropdown', 'children'),
    Input('realtime-request-btn', 'n_clicks'))
def request_realtime_data(button):
    if button and selected_ipdu:
        gc.send_message(b'GR')
    return ''

@app.callback(
    Output('waveform-select-dropdown', 'options'),
    Input('update_interval','n_intervals'))
def refresh_realtime_data(interval):
    if selected_ipdu is not None and selected_ipdu > 0:
        try:
            files = [ (path.getmtime(data_path + str(selected_ipdu) + '/' + file), file) for file in listdir(data_path + str(selected_ipdu) + '/') ]
        except:
            return []
        files.sort(key = lambda x: x[0], reverse=True)
        
        #If a new file is found, and display message to message queue
        if selected_ipdu not in rtd_count:
            rtd_count[selected_ipdu] = len(files)
        else:
            for i in range(len(files) - rtd_count[selected_ipdu]):
                strs.event_queue.append((time.ctime()[11:19], 'New realtime data received.', 'info'))
        rtd_count[selected_ipdu] = len(files)

        return [{'label': time.ctime(option[0])[:-5], 'value': option[1]} for option in files]
    return []

#Gets called once when the page first loads
@app.callback(
    Output('page-content-dummy', 'children'),
    Input('page-content-dummy', 'children'))
def page_loaded(dummy):
    global thread_started
    if not thread_started:
        thread_started = True
        print('starting thread. . .')
        threading.Thread(target=gc.start()).start()  
    return ''

@app.callback(
    Output('alarms-btn', 'className'),
    Output('alarm-count-badge', 'children'),
    Output('alarm-count-badge', 'className'),
    Input('update_interval', 'n_intervals'))
def display_alarms_btn(interval):
    return 'd-block' if mp.total_alarms else 'd-none', str(mp.unseen_alarms), 'border' if mp.unseen_alarms else 'd-none'

#Toggles alarm modal
@app.callback(
    Output("modal", "is_open"),
    [Input("open", "n_clicks"), Input("close", "n_clicks")],
    [State("modal", "is_open")],)
def toggle_modal(n1, n2, is_open):
    mp.unseen_alarms = 0
    if n1 or n2:
        return not is_open
    return is_open

#Toggles event history modal
@app.callback(
    Output("event-history-modal", "is_open"),
    [Input("notification-icon", "n_clicks"), Input("event-history-modal-close", "n_clicks")],
    [State("event-history-modal", "is_open")],)
def toggle_event_history_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open

# print('starting thread. . .')
# threading.Thread(target=childThread).start()  

if __name__ == '__main__':  
    app.run_server(debug=True, host='0.0.0.0')
