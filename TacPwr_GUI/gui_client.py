import socket
import message_parser as mp
import realtime_data_reader as rtd
import config_reader as cfg
import time
import select
import structures as strs

BYTE_FORMAT = 'little'

message_id = 1
gui_id = (112358).to_bytes(8, BYTE_FORMAT)
active_ipdu_id = 0
data_len = (0).to_bytes(4, BYTE_FORMAT)
connection_status = False
bound = False
p = 0
path_to_controller = '../controller/'
msg_id = 0

outbox = []
sock = None
inputs = None

################

def get_header(header):
    id = int.from_bytes(header[:8], BYTE_FORMAT)
    ipdu_id = int.from_bytes(header[8:16], BYTE_FORMAT)

    if ipdu_id not in strs.ipdus and ipdu_id > 0:
        strs.ipdus.append(ipdu_id)
        cfg.read(ipdu_id)
        # print(f'New IPDU connected: {ipdu_id}')
        strs.event_queue.append((time.ctime()[11:19], f'IPDU {ipdu_id} connected', 'info'))

    msg_id = int.from_bytes(header[16:20], BYTE_FORMAT)
    cmd = header[20:22].decode('utf-8')
    msg_len = int.from_bytes(header[24:28], BYTE_FORMAT)

    # return f'[{id} | {msg_id} | {cmd} | {msg_len}]'
    return id,ipdu_id,msg_id,cmd,msg_len

def send_message(msg_cmd, msg_data=b''):
    outbox.insert(0, (msg_cmd, msg_data))

#Start and run may feed into each other recursivley forever. Look into solution. TODO
def run():
    global message_id
    global connection_status
    readable = []
    writeable = []
    errors = []
    while inputs:
        try:
            readable, writeable, errors = select.select(inputs, inputs, [], 3)
        except:
            connection_status = False
            sock.close()
            inputs.remove(sock)
        for s in readable:
                try:
                    message = s.recv(28)
                except:
                    pass
                if message:
                    data_len = int.from_bytes(message[24:28], BYTE_FORMAT)
                    if data_len:
                        data = s.recv(data_len)
                    print(f'recv: {get_header(message)}')
                    # print()
                    header = get_header(message)
                    
                    message_id += 1
                    cmd = header[3]
                    if cmd == 'RS':
                        # print(f'Recieved Snapshot Data: {len(data)} bytes')

                        mp.parse_snapshot(header[1], data)
                        # send_message(b'AK00')
                    elif cmd == 'RR':
                        print(f'Recieved Realtime Data: {len(data)} bytes')

                        filename = path_to_controller + data.decode()
                        print(f'Filename: {filename}')
                        rtd.read(filename)
                        # send_message(b'AK00')
                    elif cmd == 'RA':
                        print(f'Recieved Alarm Data: {len(data)} bytes')
                        # print(header)
                        # print(print(data))
                        # print()
                        mp.parse_alarm(data, header[1])
                    elif cmd == 'RT':
                        mp.parse_temperature(data)
                    elif cmd == 'ND':
                        pass
                    elif cmd == 'AK':
                        pass
                    elif cmd == 'CV':
                        mp.parse_software(data)
                    elif cmd == 'CC':
                        pass
                    elif cmd == 'CD':
                        ipdu_id = int.from_bytes(data[:8], BYTE_FORMAT)
                        if ipdu_id in strs.ipdus:
                            strs.ipdus.remove(ipdu_id)
                            strs.event_queue.append((time.ctime()[11:19], f'IPDU {ipdu_id} disconnected', 'info'))
                    else:
                        print(f'Unknown Command: {cmd}')
                    if cmd != 'AK' and cmd != 'NK':
                        send_message(b'AK')
                else:
                    connection_status = False
                    inputs.remove(s)
                    s.close()
        if writeable and outbox:
            msg_cmd, msg_data = outbox.pop()
            if len(msg_cmd) == 2:
                msg_cmd += int(0).to_bytes(2, 'little')
            data = gui_id + active_ipdu_id.to_bytes(8, BYTE_FORMAT) + message_id.to_bytes(4, BYTE_FORMAT) + msg_cmd + len(msg_data).to_bytes(4, BYTE_FORMAT) + (msg_data if msg_data is not None else b'')

            sock.send(data)
            print(f'sent: {get_header(data)}')
            message_id+=1
        if errors:
            print('Connection error, closing socket.')
            sock.close()
            connection_status = False
    connection_status = False
def is_connected():
    return connection_status

def connect():
    global sock
    global inputs
    global connection_status
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while not connection_status:
        try:
            sock.connect(('127.0.0.1', 8090))
            connection_status = True
        except:
            print('Connection refused, retrying...')
            time.sleep(3)
    inputs = [sock]
    print('Connected to Controller')
    strs.event_queue.append((time.ctime()[11:19], f'Connected to controller', 'info'))
    send_message(b'HB')
    run()

def start():
    while True:
        connect()
        run()
