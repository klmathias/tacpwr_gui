import struct
import structures as strs
import time

ipdu_temperature = None
embedded_software_version = ''
fpga_software_version = ''

snapshot_data = {}
snapshot_history = {}


#Active alarms
alarms = {}

#Lists tripped breakers / contactors from most recent alarm event. Used to update feed control indicators and switches.
alarm_contactors = {}

#Number of alarms for each IPDU
alarm_count = {}

#Total number of active alarms
total_alarms = 0

unseen_alarms = 0

alarm_message_queue = []

#History of alarm messagse
alarm_history = {}

alarm_states = ['VOLTAGE_STATE', 'AMP_STATE', 'PHASE_STATE', 'FREQUENCY_STATE', 'BREAKER_STATE', 'CONTACTOR_STATE']
alarm_values = ['VOLTAGE_ALARM', 'AMP_ALARM', 'PHASE_ALARM', 'FREQUENCY_ALARM', 'BREAKER_ALARM', 'CONTACTOR_ALARM']
alarm_switch_states = ['BREAKER_STATE', 'CONTACTOR_STATE']
alarm_switch_values = ['BREAKER_ALARM', 'CONTACTOR_ALARM']

alarm_state_values = {
    'ALARM_CLEAR':      0,
    'ALARM_CLEARED':    1,
    'ALARM_ACTIVATED':  2,
    'ALARM_ACTIVE':     3
}

alarm_state_to_name = {
    0: 'ALARM_CLEAR',
    1: 'ALARM_CLEARED',
    2: 'ALARM_ACTIVATED',
    3: 'ALARM_ACTIVE'
}

alarm_to_name = {
    0: 'No Alarm',
    1: 'Over Alarm',
    2: 'Under Alarm',
    3: 'Open Alarm',
    4: 'Closed Alarm'
}

switch_to_name = {
    0: 'Closed',
    1: 'Closed',
    2: 'Open',
    3: 'Opened'
}

numSnapshots = 0
BYTE_FORMAT = 'little'
def parse_snapshot(ipdu, data):
    global snapshot_data
    global snapshot_history
    global numSnapshots

    if data is None or len(data) < 315:
        result = {
            #Parse contactor states
            'BYP_contactor':    False,
            '20B_contactor':    False,
            '20A_contactor':    False,
            '40B_contactor':    False,
            '40A_contactor':    False,
            '60B_contactor':    False,
            '60A_contactor':    False,
            'MAIN_contactor': False, #*******Find way to remove*****************
            #Parse circuit breaker states
            'MAIN_breaker':    False,
            '20B_breaker':    False,
            '20A_breaker':    False,
            '40B_breaker':    False,
            '40A_breaker':    False,
            '60B_breaker':    False,
            '60A_breaker':    False,
            'contactor_engaged':    False,
            'BYP_breaker': False, #*******Find way to remove*****************

            #Parse voltage/current info
            'MAIN_L1_VOLT':     -1,
            'MAIN_L2_VOLT':     -1,
            'MAIN_L3_VOLT':     -1,

            'MAIN_L1_FREQ':     -1,
            'MAIN_L2_FREQ':     -1,
            'MAIN_L3_FREQ':     -1,

            'MAIN_L1_L2_PHASE': -1,
            'MAIN_L2_L3_PHASE': -1,
            'MAIN_L3_L1_PHASE': -1,

            'MAIN_L1_AMP':      -1,
            'MAIN_L2_AMP':      -1,
            'MAIN_L3_AMP':      -1,
            'MAIN_N_AMP':       -1,

            '60A_L1_AMP':      -1,
            '60A_L2_AMP':      -1,
            '60A_L3_AMP':      -1,
            '60A_N_AMP':       -1,
            '60B_L1_AMP':      -1,
            '60B_L2_AMP':      -1,
            '60B_L3_AMP':      -1,
            '60B_N_AMP':       -1,
            '40A_L1_AMP':      -1,
            '40A_L2_AMP':      -1,
            '40A_L3_AMP':      -1,
            '40A_N_AMP':       -1,
            '40B_L1_AMP':      -1,
            '40B_L2_AMP':      -1,
            '40B_L3_AMP':      -1,
            '40B_N_AMP':       -1,
            '20A_L1_AMP':      -1,
            '20A_N_AMP':       -1,
            '20B_L2_AMP':      -1,
            '20B_N_AMP':       -1,
            'BYP_L1_AMP':      -1,
            'BYP_L2_AMP':      -1,
            'BYP_L3_AMP':      -1,
            'BYP_N_AMP':       -1,
            

            'PF_MAIN_L1':       -1,
            'PF_MAIN_L2':       -1,
            'PF_MAIN_L3':       -1,
            'PF_60A_L1':        -1,
            'PF_60A_L2':        -1,
            'PF_60A_L3':        -1,
            'PF_60B_L1':        -1,
            'PF_60B_L2':        -1,
            'PF_60B_L3':        -1,
            'PF_40A_L1':        -1,
            'PF_40A_L2':        -1,
            'PF_40A_L3':        -1,
            'PF_40B_L1':        -1,
            'PF_40B_L2':        -1,
            'PF_40B_L3':        -1,
            'PF_20A_L1':        -1,
            'PF_20B_L2':        -1,
            'PF_BYP_L1':        -1,
            'PF_BYP_L2':        -1,
            'PF_BYP_L3':        -1,

            
            'ID': 0,

            #Read timestap data
            'TIMESTAMP_DATA_ID':-1,
            'TIME_EPOC_SECONDS':-1,
            'TIME_NANO': -1,

            '20A_L2_AMP': -1,
            '20A_L3_AMP': -1,
            
            '20B_L1_AMP': -1,
            '20B_L3_AMP': -1,
        }
    else:
        contactors = int.from_bytes(data[:4], BYTE_FORMAT)
        breakers = int.from_bytes(data[4:8], BYTE_FORMAT)
        indicies = range(8,476,8)
        result={
            #Parse contactor states
            'BYP_contactor': (contactors & (1 << 0)) != 0,
            '20B_contactor':    (contactors & (1 << 1)) != 0,
            '20A_contactor':    (contactors & (1 << 2)) != 0,
            '40B_contactor':    (contactors & (1 << 3)) != 0,
            '40A_contactor':    (contactors & (1 << 4)) != 0,
            '60B_contactor':    (contactors & (1 << 5)) != 0,
            '60A_contactor':    (contactors & (1 << 6)) != 0,
            'MAIN_contactor': False, #*******Find way to remove*****************
            #Parse circuit breaker states
            'MAIN_breaker':   (breakers & (1 << 0)) != 0,
            '20B_breaker':      (breakers & (1 << 1)) != 0,
            '20A_breaker':      (breakers & (1 << 2)) != 0,
            '40B_breaker':      (breakers & (1 << 3)) != 0,
            '40A_breaker':      (breakers & (1 << 4)) != 0,
            '60B_breaker':      (breakers & (1 << 5)) != 0,
            '60A_breaker':      (breakers & (1 << 6)) != 0,
            'contactor_engaged':(breakers & (1 << 16)) != 0,
            'BYP_breaker': False, #*******Find way to remove*****************

            #Parse voltage/current info
            'MAIN_L1_VOLT':     struct.unpack('d', data[indicies[0]:indicies[1]])[0],
            'MAIN_L2_VOLT':     struct.unpack('d', data[indicies[1]:indicies[2]])[0],
            'MAIN_L3_VOLT':     struct.unpack('d', data[indicies[2]:indicies[3]])[0],

            'MAIN_L1_FREQ':     struct.unpack('d', data[indicies[3]:indicies[4]])[0],
            'MAIN_L2_FREQ':     struct.unpack('d', data[indicies[4]:indicies[5]])[0],
            'MAIN_L3_FREQ':     struct.unpack('d', data[indicies[5]:indicies[6]])[0],

            'MAIN_L1_L2_PHASE': struct.unpack('d', data[indicies[6]:indicies[7]])[0],
            'MAIN_L2_L3_PHASE': struct.unpack('d', data[indicies[7]:indicies[8]])[0],
            'MAIN_L3_L1_PHASE': struct.unpack('d', data[indicies[8]:indicies[9]])[0],

            'MAIN_L1_AMP':      struct.unpack('d', data[indicies[9]:indicies[10]])[0],
            'MAIN_L2_AMP':      struct.unpack('d', data[indicies[10]:indicies[11]])[0],
            'MAIN_L3_AMP':      struct.unpack('d', data[indicies[11]:indicies[12]])[0],
            'MAIN_N_AMP':       struct.unpack('d', data[indicies[12]:indicies[13]])[0],

            '60A_L1_AMP':      struct.unpack('d', data[indicies[13]:indicies[14]])[0],
            '60A_L2_AMP':      struct.unpack('d', data[indicies[14]:indicies[15]])[0],
            '60A_L3_AMP':      struct.unpack('d', data[indicies[15]:indicies[16]])[0],
            '60A_N_AMP':       struct.unpack('d', data[indicies[16]:indicies[17]])[0],
            '60B_L1_AMP':      struct.unpack('d', data[indicies[17]:indicies[18]])[0],
            '60B_L2_AMP':      struct.unpack('d', data[indicies[18]:indicies[19]])[0],
            '60B_L3_AMP':      struct.unpack('d', data[indicies[19]:indicies[20]])[0],
            '60B_N_AMP':       struct.unpack('d', data[indicies[20]:indicies[21]])[0],
            '40A_L1_AMP':      struct.unpack('d', data[indicies[21]:indicies[22]])[0],
            '40A_L2_AMP':      struct.unpack('d', data[indicies[22]:indicies[23]])[0],
            '40A_L3_AMP':      struct.unpack('d', data[indicies[23]:indicies[24]])[0],
            '40A_N_AMP':       struct.unpack('d', data[indicies[24]:indicies[25]])[0],
            '40B_L1_AMP':      struct.unpack('d', data[indicies[25]:indicies[26]])[0],
            '40B_L2_AMP':      struct.unpack('d', data[indicies[26]:indicies[27]])[0],
            '40B_L3_AMP':      struct.unpack('d', data[indicies[27]:indicies[28]])[0],
            '40B_N_AMP':       struct.unpack('d', data[indicies[28]:indicies[29]])[0],
            '20A_L1_AMP':      struct.unpack('d', data[indicies[29]:indicies[30]])[0],
            '20A_N_AMP':       struct.unpack('d', data[indicies[30]:indicies[31]])[0],
            '20B_L2_AMP':      struct.unpack('d', data[indicies[31]:indicies[32]])[0],
            '20B_N_AMP':       struct.unpack('d', data[indicies[32]:indicies[33]])[0],
            'BYP_L1_AMP':      struct.unpack('d', data[indicies[33]:indicies[34]])[0],
            'BYP_L2_AMP':      struct.unpack('d', data[indicies[34]:indicies[35]])[0],
            'BYP_L3_AMP':      struct.unpack('d', data[indicies[35]:indicies[36]])[0],
            'BYP_N_AMP':       struct.unpack('d', data[indicies[36]:indicies[37]])[0],

            'PF_MAIN_L1':       struct.unpack('d', data[indicies[37]:indicies[38]])[0],
            'PF_MAIN_L2':       struct.unpack('d', data[indicies[38]:indicies[39]])[0],
            'PF_MAIN_L3':       struct.unpack('d', data[indicies[39]:indicies[40]])[0],
            'PF_60A_L1':        struct.unpack('d', data[indicies[40]:indicies[41]])[0],
            'PF_60A_L2':        struct.unpack('d', data[indicies[41]:indicies[42]])[0],
            'PF_60A_L3':        struct.unpack('d', data[indicies[42]:indicies[43]])[0],
            'PF_60B_L1':        struct.unpack('d', data[indicies[43]:indicies[44]])[0],
            'PF_60B_L2':        struct.unpack('d', data[indicies[44]:indicies[45]])[0],
            'PF_60B_L3':        struct.unpack('d', data[indicies[45]:indicies[46]])[0],
            'PF_40A_L1':        struct.unpack('d', data[indicies[46]:indicies[47]])[0],
            'PF_40A_L2':        struct.unpack('d', data[indicies[47]:indicies[48]])[0],
            'PF_40A_L3':        struct.unpack('d', data[indicies[48]:indicies[49]])[0],
            'PF_40B_L1':        struct.unpack('d', data[indicies[49]:indicies[50]])[0],
            'PF_40B_L2':        struct.unpack('d', data[indicies[50]:indicies[51]])[0],
            'PF_40B_L3':        struct.unpack('d', data[indicies[51]:indicies[52]])[0],
            'PF_20A_L1':        struct.unpack('d', data[indicies[52]:indicies[53]])[0],
            'PF_20B_L2':        struct.unpack('d', data[indicies[53]:indicies[54]])[0],
            'PF_BYP_L1':        struct.unpack('d', data[indicies[54]:indicies[55]])[0],
            'PF_BYP_L2':        struct.unpack('d', data[indicies[55]:indicies[56]])[0],
            'PF_BYP_L3':        struct.unpack('d', data[indicies[56]:indicies[57]])[0],

            #Read timestap data
            # 'TIMESTAMP_DATA_ID':data[indicies[34]:indicies[35]],
            'TIME_LOW': 	data[indicies[38]:indicies[39]],
            'TIME_NANO':	data[indicies[39]:indicies[40]],

            'ID': numSnapshots,

            '20A_L2_AMP': -1,
            '20A_L3_AMP': -1,
            
            '20B_L1_AMP': -1,
            '20B_L3_AMP': -1,
        }
    snapshot_data[ipdu] = result

    if ipdu in snapshot_history and snapshot_history[ipdu]:
        for key in snapshot_data[ipdu]:
            snapshot_history[ipdu][key].append(snapshot_data[ipdu][key])
            if len(snapshot_history[ipdu][key]) > 25:
                snapshot_history[ipdu][key] = snapshot_history[ipdu][key][-25:]
    else:
        snapshot_history[ipdu] = {}
        for key in snapshot_data[ipdu]:
            snapshot_history[ipdu][key] = [snapshot_data[ipdu][key]]
    numSnapshots += 1
    return result

def parse_alarm(data, ipdu):
    global alarm_count
    global total_alarms
    global unseen_alarms
    result = {}
    
    if len(data) < 128:
        return

    values = struct.unpack('64s16I', data[0:128])
    result = {
        'CIRCUIT_NAME':      values[0].decode(),
        'VOLTAGE_STATE':     values[1],
        'VOLTAGE_ALARM':     values[2],
        'AMP_STATE':         values[3],
        'AMP_ALARM':         values[4],
        'PHASE_STATE':       values[5],
        'PHASE_ALARM':       values[6],
        'FREQUENCY_STATE':   values[7],
        'FREQUENCY_ALARM':   values[8],
        'BREAKER_STATE':     values[9],
        'BREAKER_ALARM':     values[10],
        'CONTACTOR_STATE':   values[11],
        'CONTACTOR_ALARM':   values[12],
        'ALARM_VALUE':       values[13],
        'TIME_HIGH':        values[14],
        'TIME_LOW':        values[15]
    }

    circuit = str(result['CIRCUIT_NAME']).rstrip('\x00').lstrip('F')
    switch_name = circuit.split('_')[0]
    alarm_time = time.ctime(result['TIME_LOW'])[11:19]


    

    if not ipdu in alarm_history:
        alarm_history[ipdu] = []
    if not ipdu in alarms:
        alarms[ipdu] = {}
        alarm_count[ipdu] = 0
    if not circuit in alarms[ipdu]:
        alarms[ipdu][circuit] = {}

    alarm_value = result['ALARM_VALUE']

    print(circuit)
    for i in range(len(alarm_states)):
        state = alarm_states[i]
        value = alarm_values[i]
        print(f'{state:} {alarm_state_to_name[result[state]]} -> {alarm_to_name[result[value]]}')
        if result[state] == 0:
            if state in alarms[ipdu][circuit]:
                # strs.event_queue.append((alarm_time, f'{circuit} {alarm_to_name[result[value]]}: {alarm_value}', 'success'))
                del alarms[ipdu][circuit][state]
                total_alarms -= 1
                alarm_count[ipdu] -= 1
        elif result[state] == 1:
            if state in alarms[ipdu][circuit]:
                strs.event_queue.append((alarm_time, f'{circuit} {alarm_to_name[result[value]]}: {alarm_value}', 'success'))
                del alarms[ipdu][circuit][state]
                total_alarms -= 1
                alarm_count[ipdu] -= 1
        elif result[state] == 2:
            if not state in alarms[ipdu][circuit]:
                total_alarms += 1
                unseen_alarms += 1
                alarm_count[ipdu] += 1
            alarms[ipdu][circuit][state] = f'{alarm_to_name[result[value]]}: {alarm_value}'
            strs.event_queue.append((alarm_time, f'{circuit} {alarm_to_name[result[value]]}: {alarm_value}', 'danger'))
        elif result[state] == 3:
            if not state in alarms[ipdu][circuit]:
                total_alarms += 1
                unseen_alarms += 1
                alarm_count[ipdu] += 1
            alarms[ipdu][circuit][state] = f'{alarm_to_name[result[value]]}: {alarm_value}'
            # strs.event_queue.append((alarm_time, f'{circuit} {alarm_to_name[result[value]]}: {alarm_value}', 'danger'))

    for i in range(len(alarm_switch_states)):
        state = alarm_switch_states[i]
        value = alarm_switch_values[i]
        if result[state] == 0:
            if state in alarms[ipdu][circuit]:
                # strs.event_queue.append((alarm_time, f'{circuit} {alarm_to_name[result[value]]}: {alarm_value}', 'success'))
                del alarms[ipdu][circuit][state]
                total_alarms -= 1
                alarm_count[ipdu] -= 1
        elif result[state] == 1:
            if state in alarms[ipdu][circuit]:
                strs.event_queue.append((alarm_time, f'{alarm_to_name[result[value]]}: {alarm_value}', 'success'))
                del alarms[ipdu][circuit][state]
                total_alarms -= 1
                alarm_count[ipdu] -= 1
        elif result[state] == 2:
            if not state in alarms[ipdu][circuit]:    
                total_alarms += 1
                unseen_alarms += 1
                alarm_count[ipdu] += 1
            alarms[ipdu][circuit][state] = f'{switch_to_name[result[value]]}: {state}'
            if state == 'CONTACTOR_STATE':
                if ipdu not in alarm_contactors:
                    alarm_contactors[ipdu] = []
                alarm_contactors[ipdu].append(switch_name)
            strs.event_queue.append((alarm_time, f'{circuit} {alarm_to_name[result[value]]}: {alarm_value}', 'danger'))
        elif result[state] == 3:
            if not state in alarms[ipdu][circuit]:
                total_alarms += 1
                unseen_alarms += 1
                alarm_count[ipdu] += 1
            alarms[ipdu][circuit][state] = f'{switch_to_name[result[value]]}: {state}'
            strs.event_queue.append((alarm_time, f'{circuit} {alarm_to_name[result[value]]}: {alarm_value}', 'danger'))
    print('------------------------------------')
    alarm_history[ipdu].append(alarms[ipdu])

def parse_temperature(data):
    global ipdu_temperature
    ipdu_temperature = struct.unpack('i', data[0:4])[0]

def parse_software(data):
    global embedded_software_version
    global fpga_software_version
    try:
        embedded_software_version, fpga_software_version = tuple(data.decode().split(';'))
    except:
        # print('Error parsing software version')
        pass