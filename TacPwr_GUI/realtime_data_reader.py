import numpy as np
import pandas as pd
import csv

realtime_data = {}
rms = {}
sample_rate = 25
filename = ''

def rowfilter(val):
    if val == 0:
        return True
    return val % sample_rate != 0
def calc_rms(col):
    return round(np.sqrt(col.pow(2).mean()),2)

def involved_events(file):
    if file is None or len(file) == 0:
        return None
    return pd.read_csv(file, nrows=0).columns

def read(file):
    if file is None or len(file) == 0:
        return
    global filename
    global realtime_data
    global rms
    # full_data = pd.read_csv('Real-Time-Data.csv', skiprows=rowfilter)
    display_data = pd.read_csv(file, skiprows=rowfilter)
    filename = file
    rms = {
        'MAIN_L1_VOLT': calc_rms(display_data[display_data.columns[0]]),
        'MAIN_L2_VOLT': calc_rms(display_data[display_data.columns[1]]),
        'MAIN_L3_VOLT': calc_rms(display_data[display_data.columns[2]]),

        # 'FNOISE': calc_rms(display_data[display_data.columns[3]]),

        'MAIN_L1_AMP': calc_rms(display_data[display_data.columns[4]]),
        'MAIN_L2_AMP': calc_rms(display_data[display_data.columns[5]]),
        'MAIN_L3_AMP': calc_rms(display_data[display_data.columns[6]]),
        'MAIN_N_AMP': calc_rms(display_data[display_data.columns[7]]),
        
        '60A_L1_AMP': calc_rms(display_data[display_data.columns[8]]),
        '60A_L2_AMP': calc_rms(display_data[display_data.columns[9]]),
        '60A_L3_AMP': calc_rms(display_data[display_data.columns[10]]),
        '60A_N_AMP': calc_rms(display_data[display_data.columns[11]]),
        
        '60B_L1_AMP': calc_rms(display_data[display_data.columns[12]]),
        '60B_L2_AMP': calc_rms(display_data[display_data.columns[13]]),
        '60B_L3_AMP': calc_rms(display_data[display_data.columns[14]]),
        '60B_N_AMP': calc_rms(display_data[display_data.columns[15]]),
        
        '40A_L1_AMP': calc_rms(display_data[display_data.columns[16]]),
        '40A_L2_AMP': calc_rms(display_data[display_data.columns[17]]),
        '40A_L3_AMP': calc_rms(display_data[display_data.columns[18]]),
        '40A_N_AMP': calc_rms(display_data[display_data.columns[19]]),
        
        '40B_L1_AMP': calc_rms(display_data[display_data.columns[20]]),
        '40B_L2_AMP': calc_rms(display_data[display_data.columns[21]]),
        '40B_L3_AMP': calc_rms(display_data[display_data.columns[22]]),
        '40B_N_AMP': calc_rms(display_data[display_data.columns[23]]),
        
        '20A_L1_AMP': calc_rms(display_data[display_data.columns[24]]),
        '20A_N_AMP': calc_rms(display_data[display_data.columns[25]]),
        
        '20B_L2_AMP': calc_rms(display_data[display_data.columns[26]]),
        '20B_N_AMP': calc_rms(display_data[display_data.columns[27]]),
        
        'BYP_L1_AMP': calc_rms(display_data[display_data.columns[28]]),
        'BYP_L2_AMP': calc_rms(display_data[display_data.columns[29]]),
        'BYP_L3_AMP': calc_rms(display_data[display_data.columns[30]]),
        'BYP_N_AMP': calc_rms(display_data[display_data.columns[31]]),
        
        # 'DEADBEEF': calc_rms(display_data[display_data.columns[32]]),

        
        # 'TIME_HIGH': calc_rms(display_data[display_data.columns[33]]),
        # 'TIME_LOW': calc_rms(display_data[display_data.columns[34]]),
        # 'TIME_NANO': calc_rms(display_data[display_data.columns[35]]),

        
        '20A_L2_AMP': [0],
        '20A_L3_AMP': [0],
        
        '20B_L1_AMP': [0],
        '20B_L3_AMP': [0],
    }
    realtime_data = {
        'MAIN_L1_VOLT': display_data[display_data.columns[0]],
        'MAIN_L2_VOLT': display_data[display_data.columns[1]],
        'MAIN_L3_VOLT': display_data[display_data.columns[2]],

        'NOISE': display_data[display_data.columns[3]],

        'MAIN_L1_AMP': display_data[display_data.columns[4]],
        'MAIN_L2_AMP': display_data[display_data.columns[5]],
        'MAIN_L3_AMP': display_data[display_data.columns[6]],
        'MAIN_N_AMP': display_data[display_data.columns[7]],
        
        '60A_L1_AMP': display_data[display_data.columns[8]],
        '60A_L2_AMP': display_data[display_data.columns[9]],
        '60A_L3_AMP': display_data[display_data.columns[10]],
        '60A_N_AMP': display_data[display_data.columns[11]],
        
        '60B_L1_AMP': display_data[display_data.columns[12]],
        '60B_L2_AMP': display_data[display_data.columns[13]],
        '60B_L3_AMP': display_data[display_data.columns[14]],
        '60B_N_AMP': display_data[display_data.columns[15]],
        
        '40A_L1_AMP': display_data[display_data.columns[16]],
        '40A_L2_AMP': display_data[display_data.columns[17]],
        '40A_L3_AMP': display_data[display_data.columns[18]],
        '40A_N_AMP': display_data[display_data.columns[19]],
        
        '40B_L1_AMP': display_data[display_data.columns[20]],
        '40B_L2_AMP': display_data[display_data.columns[21]],
        '40B_L3_AMP': display_data[display_data.columns[22]],
        '40B_N_AMP': display_data[display_data.columns[23]],
        
        '20A_L1_AMP': display_data[display_data.columns[24]],
        '20A_N_AMP': display_data[display_data.columns[25]],
        
        '20B_L2_AMP': display_data[display_data.columns[26]],
        '20B_N_AMP': display_data[display_data.columns[27]],
        
        'BYP_L1_AMP': display_data[display_data.columns[28]],
        'BYP_L2_AMP': display_data[display_data.columns[29]],
        'BYP_L3_AMP': display_data[display_data.columns[30]],
        'BYP_N_AMP': display_data[display_data.columns[31]],
        
        'DEADBEEF': display_data[display_data.columns[32]],

        
        'TIME_HIGH': display_data[display_data.columns[33]],
        'TIME_LOW': display_data[display_data.columns[34]],
        'TIME_NANO': display_data[display_data.columns[35]],

        
        '20A_L2_AMP': [0],
        '20A_L3_AMP': [0],
        
        '20B_L1_AMP': [0],
        '20B_L3_AMP': [0],
    }
    return realtime_data
